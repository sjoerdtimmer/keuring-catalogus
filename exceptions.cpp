#include "exceptions.h"



QString CustomException::what()
{
    return s;
}

bool CustomException::isCritical()
{
    return critical;
}

QStyle::StandardPixmap CustomException::iconstyle()
{
    return pixmap;
}

CustomException::CustomException(QString ss)
{
    s = ss;
    critical = true;
    pixmap = QStyle::SP_MessageBoxCritical;
}

CustomWarning::CustomWarning(QString ss) : CustomException (ss)
{
    critical = false;
    pixmap = QStyle::SP_MessageBoxWarning;
}

CustomNotice::CustomNotice(QString ss) : CustomException (ss)
{
    critical = false;
    pixmap = QStyle::SP_MessageBoxInformation;
}

CustomAcceptance::CustomAcceptance(QString ss) : CustomException (ss)
{
    critical = false;
    pixmap = QStyle::SP_DialogApplyButton;
}
