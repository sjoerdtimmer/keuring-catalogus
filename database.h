#ifndef DATABASE_H
#define DATABASE_H

#include "datastructures/show.h"

#include <QObject>
#include <QFile>

class Database : public QObject
{
    Q_OBJECT
private:
    Database(); // private constructor prevents accidental creation outside instance()
    
       
       
public:
//    static Database & instance();
    static Database * instance() {
        static Database * _instance = 0;
        if ( _instance == 0 ) {
            _instance = new Database();
        }
        return _instance;
    }

    void saveShows();
    void loadShows();
    static QString sanitizeShowFileName(Show &show);
    QString nextAvailableName();

    //static void loadSheepFromStream(QTextStream  &in, QHash<QString,Breeder> &breederlist, QHash<QString,Sheep> &sheeplist, QString delimiter=",");

//    QStringList * uniqueOwners(Show*);

    QList<Show> shows;

signals:
    void showRead(Show& show);

};

#endif // DATABASE_H
