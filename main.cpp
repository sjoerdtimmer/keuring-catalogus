#include "mainwindow.h"
#include <QApplication>
#include <QLocale>
#include <QTranslator>
#include <QDebug>
#include <QLibraryInfo>
#include <iostream>
#include <fstream>


void myMessageOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    switch (type) {
    case QtDebugMsg:
        fprintf(stdout, "Debug: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        break;
    case QtInfoMsg:
        fprintf(stdout, "Info: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        break;
    case QtWarningMsg:
        fprintf(stdout, "Warning: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        break;
    case QtCriticalMsg:
        fprintf(stdout, "Critical: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        break;
    case QtFatalMsg:
        fprintf(stdout, "Fatal: %s (%s:%u, %s)\n", localMsg.constData(), context.file, context.line, context.function);
        fflush(stdout);
        abort();
    }
}

int main(int argc, char *argv[])
{
    
    //std::freopen ("stdout.txt","w",stdout);
    //std::freopen ("stderr.txt","w",stderr);
    
    
    qInstallMessageHandler(myMessageOutput);
    
    std::cout << "Hello via stdout" << std::endl;
    qDebug() << "Hello from Qt!";
    
    QLocale::setDefault(QLocale(QLocale::Dutch, QLocale::Netherlands));
//    QLocale::setDefault(QLocale::Dutch);
    QTranslator dutch;
//    dutch.load("NL_nl", QLibraryInfo::location(QLibraryInfo::TranslationsPath));
//    QApplication::instance()->installTranslator(&dutch);
    
    
    QApplication a(argc, argv);
    MainWindow w;
    w.show();

    return a.exec();
}
