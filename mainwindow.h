#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QStringList>
#include <QSortFilterProxyModel>
#include <QListWidgetItem>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_pushButton_add_show_clicked();

    void on_pushButton_quit_clicked();

    void on_show_added();

    void on_show_selectionChanged(const QListWidgetItem *current, const QListWidgetItem *previous);

    void on_pushButton_remove_show_clicked();

    void on_pushButton_manage_registrations_clicked();

    void on_pushButton_clicked();

    void on_pushButton_export_invitations_clicked();

    void on_pushButton_manage_catagories_clicked();

    void on_pushButton_export_categories_clicked();
    
    void on_pushButton_edit_show_clicked();
    
    void on_pushButton_manage_results_clicked();
    
    void on_pushButton_export_stable_inventory_clicked();
    
private:
    Ui::MainWindow * ui;
//    QStringList * keuringen;
//    QSortFilterProxyModel *proxyModel;
//    KeuringenList *keuringenlist;
};

#endif // MAINWINDOW_H
