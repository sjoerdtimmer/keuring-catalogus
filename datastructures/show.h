#ifndef SHOW_H
#define SHOW_H

#include "datastructures/sheep.h"
#include "datastructures/breeder.h"

#include <QObject>
#include <QExplicitlySharedDataPointer>
#include <QListWidgetItem>
#include <QDate>

class ShowData;
class ShowListItem;

class Show
{
public:
    Show();
    Show(const Show &);
    Show &operator=(const Show &);
    ~Show();
    
    void setName(QString name);
    void setDate(QDate date);
    void setSheep(QHash<QString,Sheep> sheep);
    void setBreeders(QHash<QString,Breeder> breeders);
    
    QString name() const;
    QDate date() const;
    QHash<QString,Sheep> sheep() const;
    QHash<QString,Breeder> breeders() const;
    
//    bool operator <(const QListWidgetItem &other) const;
    
    bool operator ==(const QListWidgetItem &other) const;
    bool operator == (const Show &other) const;
    
private:
    QExplicitlySharedDataPointer<ShowData> data;
    ShowListItem * listitem = nullptr;
    friend class ShowListItem; // allow showlist item to access private members
};

// 
class ShowListItem : public QListWidgetItem
{
public:
    ShowListItem(Show& s);
    ~ShowListItem();
    Show show;
    bool operator < (const QListWidgetItem &other) const;
    void updateText();
};

Q_DECLARE_METATYPE(Show);


QDataStream & operator<< (QDataStream& stream, const Show& s);
QDataStream & operator>> (QDataStream& stream, Show& s);



#endif // SHOW_H
