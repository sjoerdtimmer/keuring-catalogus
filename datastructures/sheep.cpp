#include "datastructures/sheep.h"
#include <QDataStream>
#include <QObject>
#include <QRegularExpression>
#include <QtDebug>

class SheepData : public QSharedData
{
public:
    // a string to represent the unique id:
    QString stn;
    QString uln;

    // info about sheep:
    QString owner;
    QString breeder;

    QDate dateOfBirth;
    QString father;
    QString mother;
    
    Sheep::Gender gender;

    // info about participation:
    bool doesParticipate;
    bool doesCompanyThree;
    bool doesPairOfRam;
    bool doesTripleOfRam;
    QString childPresentation; // empty or name of child
    int subcategory; // current group assignment
    int order;
    
    // info about results:
    Sheep::Rank rank;
    int place;// 1 voor A1/B1/C1; 2 voor A2/B2/C2 etc.
    QString comment;
    QString tailindex;
    QString shortname;
};

Sheep::Sheep() : data(new SheepData)
{
//    qDebug() << QObject::tr("constructing a new sheep");
    reset(); 
}

Sheep::Sheep(const Sheep &rhs)  : data(rhs.data)
{
//    qDebug() << "this data: " << this->data;
//    qDebug() << "other data: " << rhs.data;
//    this->data = rhs.data;
//    qDebug() << QObject::tr("copy constructing a new sheep");
}

Sheep &Sheep::operator=(const Sheep &rhs)
{
//    qDebug() << QObject::tr("assignment of sheep");
    if (this->data != rhs.data)
        data.operator=(rhs.data);
    return *this;
}

Sheep Sheep::clone() {
//    qDebug() << "cloning sheel: "<< this->shortName() << "/" << this->id();
    Sheep newsheep = Sheep();
    newsheep.data = this->data;
    newsheep.data.detach();
//    qDebug() << "sheep cloned: "<< newsheep.shortName() << "/" << newsheep.id();
    return newsheep;
}

Sheep::~Sheep()
{
    
}

void Sheep::reset() {
    setRank(Sheep::RankNone);
    setOrder(-1);
    setPlace(-1);
    setSubcategory(0);
    setDoesPairOfRam(false);
    setDoesParticipate(false);
    setDoesTripleOfRam(false);
    setDoesCompanyThree(false);
    setChildPresentation("");
    setComment("");
}

QString Sheep::stn() const { return data->stn; }
QString Sheep::uln() const { return data->uln; }
QString Sheep::owner() const { return data->owner; }
QString Sheep::breeder() const { return data->breeder; }
QDate Sheep::dateOfBirth() const { return data->dateOfBirth; }
QString Sheep::father() const { return data->father; }
QString Sheep::mother() const { return data->mother; }
Sheep::Gender Sheep::gender() const { return data->gender; }
bool Sheep::doesParticipate() const {return data->doesParticipate; }
bool Sheep::doesCompanyThree() const { return data->doesCompanyThree; }
bool Sheep::doesPairOfRam() const { return data->doesPairOfRam; }
bool Sheep::doesTripleOfRam() const { return data->doesTripleOfRam; }
QString Sheep::childPresentation() const { return data->childPresentation; }
int Sheep::subcategory() const { return data->subcategory; }
int Sheep::order() const { return data->order; }
Sheep::Rank Sheep::rank() const {return data->rank; }
int Sheep::place() const {return data->place; }
QString Sheep::comment() const { return data->comment; }
QString Sheep::tailindex() const {return data->tailindex; }

QString Sheep::shortName(QString s) {
    QRegularExpression re = QRegularExpression("^NL0*(.*)$");
    QString shortname = re.match(s).captured(1).trimmed();
    if (shortname.length() > 0)
    {
        return shortname;
    } else {
        // fall back to un-truncated stn
        return s;
    }
}




void Sheep::setStn(QString stn) {data->stn = stn; }
void Sheep::setUln(QString uln) {data->uln = uln; }
void Sheep::setOwner(QString breeder) { data->owner = breeder; }
void Sheep::setBreeder(QString breeder) { data->breeder = breeder; }
void Sheep::setDateOfBirth(QDate date) { data->dateOfBirth = date; }
void Sheep::setFather(QString father) { data->father = father; }
void Sheep::setMother(QString mother) { data->mother = mother; }
void Sheep::setGender(Sheep::Gender gender) { data->gender = gender; }
void Sheep::setDoesParticipate(bool state) { data->doesParticipate = state; }
void Sheep::setDoesCompanyThree(bool state) { data->doesCompanyThree = state; }
void Sheep::setDoesPairOfRam(bool state) { data->doesPairOfRam = state; }
void Sheep::setDoesTripleOfRam(bool state) { data->doesTripleOfRam = state; }
void Sheep::setChildPresentation(QString childName) { data->childPresentation = childName; }
void Sheep::setSubcategory(int subcategory) { data->subcategory = subcategory; }
void Sheep::setOrder(int order) { data->order = order; }
void Sheep::setRank(Sheep::Rank rank) { data->rank = rank; }
void Sheep::setPlace(int place) { data->place = place; }
void Sheep::setComment(QString comment) { data->comment = comment; }
void Sheep::setTailindex(QString index) {data->tailindex = index; }


QDataStream & operator<< (QDataStream& stream, const Sheep& s) 
{
    stream << QString("v1");
    stream << s.stn();
    stream << s.uln();
    stream << s.owner(); 
    stream << s.breeder(); 
    stream << s.dateOfBirth(); 
    stream << s.father(); 
    stream << s.mother(); 
    
    stream << (quint32)s.gender();
    stream << s.doesParticipate(); 
    stream << s.doesCompanyThree(); 
    stream << s.doesPairOfRam(); 
    stream << s.doesTripleOfRam();
    stream << s.childPresentation(); 
    stream << s.subcategory(); 
    stream << s.order(); 
    stream << (quint32)s.rank(); 
    stream << s.place();
    stream << s.comment();
    stream << s.tailindex();
    return stream;
}
QDataStream & operator>> (QDataStream&stream, Sheep& s)
{
    int version = 0;
    QString tmp_str;
    QDate tmp_date;
    quint32 tmp_int;
    bool tmp_bool;
    
    stream >> tmp_str;
    if (! tmp_str.compare("v1")) {
        version = 1;
    }
    
    // in version 0, there is no version header and the first item is the uln
    if (version==0) {
        s.setUln(tmp_str);
    } 
    
    // in version 1, there are two fields for stn and uln besides the version number
    if (version==1) {
        stream >> tmp_str;
        s.setStn(tmp_str);
        stream >> tmp_str;
        s.setUln(tmp_str);
    }
    stream >> tmp_str; s.setOwner(tmp_str);
    stream >> tmp_str; s.setBreeder(tmp_str);
    stream >> tmp_date; s.setDateOfBirth(tmp_date);
    stream >> tmp_str; s.setFather(tmp_str);
    stream >> tmp_str; s.setMother(tmp_str);
    stream >> tmp_int; s.setGender((Sheep::Gender)tmp_int);
    stream >> tmp_bool; s.setDoesParticipate(tmp_bool);
    stream >> tmp_bool; s.setDoesCompanyThree(tmp_bool);
    stream >> tmp_bool; s.setDoesPairOfRam(tmp_bool);
    stream >> tmp_bool; s.setDoesTripleOfRam(tmp_bool);
    stream >> tmp_str; s.setChildPresentation(tmp_str);
    stream >> tmp_int; s.setSubcategory(tmp_int);
    stream >> tmp_int; s.setOrder(tmp_int);
    stream >> tmp_int; s.setRank((Sheep::Rank)tmp_int);
    stream >> tmp_int; s.setPlace(tmp_int);
    stream >> tmp_str; s.setComment(tmp_str);
    stream >> tmp_str; s.setTailindex(tmp_str);
    if (version < 1) { // in version 0 there was a "friendly name" that resembles a stn at the end
        stream >> tmp_str; s.setStn(tmp_str);
    }
    return stream;
}


