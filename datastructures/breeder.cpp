//#include "breeder.h"
#include "datastructures/breeder.h"

#include <QDebug>

class BreederData : public QSharedData
{
public:
    QString id;
    QString title;
    QString initials;
    QString prefixes;
    QString name;
    QString address;
    QString postalcode;
    QString city;
    QString abbrv;
    QString email;
    QString phone;
    QString region;
};

Breeder::Breeder() : data(new BreederData)
{
    
}

Breeder::Breeder(const Breeder &rhs) : data(rhs.data)
{
    
}

Breeder &Breeder::operator=(const Breeder &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

Breeder::~Breeder()
{
    
}

QString Breeder::id() const { return data->id ;}
QString Breeder::title() const { return data->title ;}
QString Breeder::initials() const { return data->initials ;}
QString Breeder::prefixes() const { return data->prefixes ;}
QString Breeder::name() const { return data->name ;}
QString Breeder::address() const { return data->address ;}
QString Breeder::postalcode() const { return data->postalcode ;}
QString Breeder::city() const { return data->city ;}
QString Breeder::abbrv() const { return data->abbrv;}
QString Breeder::email() const { return data->email;}
QString Breeder::phone() const { return data->phone;}
QString Breeder::region() const { return data->region;}


void Breeder::setId(QString id) { data->id = id; }
void Breeder::setTitle(QString title) { data->title = title; }
void Breeder::setInitials(QString initials) { data->initials = initials; }
void Breeder::setPrefixes(QString prefix) { data->prefixes = prefix; }
void Breeder::setName(QString name) { data->name = name; }
void Breeder::setAddress(QString addres) { data->address = addres; }
void Breeder::setpostalcode(QString code) { data->postalcode = code; }
void Breeder::setCity(QString city) { data->city = city; }
void Breeder::setAbbrv(QString abbrv) { data->abbrv = abbrv; }
void Breeder::setEmail(QString email) { data->email = email; }
void Breeder::setPhone(QString phone) { data->phone = phone; }
void Breeder::setRegion(QString region) { data->region = region; }


QDataStream & operator<< (QDataStream& stream, const Breeder& b)
{
    stream << b.id() 
           << b.title() 
           << b.initials() 
           << b.prefixes() 
           << b.name() 
           << b.address() 
           << b.postalcode()
           << b.city()
           << b.abbrv()
           << b.email()
           << b.phone()
           << b.region();
    return stream;
}
QDataStream & operator>> (QDataStream& stream, Breeder& b)
{
    QString tmp;
    stream >> tmp; b.setId(tmp);
    stream >> tmp; b.setTitle(tmp);
    stream >> tmp; b.setInitials(tmp);
    stream >> tmp; b.setPrefixes(tmp);
    stream >> tmp; b.setName(tmp);
    stream >> tmp; b.setAddress(tmp);
    stream >> tmp; b.setpostalcode(tmp);
    stream >> tmp; b.setCity(tmp);
    stream >> tmp; b.setAbbrv(tmp);
    stream >> tmp; b.setEmail(tmp);
    stream >> tmp; b.setPhone(tmp);
    stream >> tmp; b.setRegion(tmp);
    return stream;
}
