#ifndef BREEDER_H
#define BREEDER_H

#include <QObject>
#include <QExplicitlySharedDataPointer>
#include <QString>

class BreederData;

class Breeder
{
public:
    Breeder();
    Breeder(const Breeder &);
    Breeder &operator=(const Breeder &);
    ~Breeder();
    
    QString id() const;
    QString title() const;
    QString initials() const;
    QString prefixes() const;
    QString name() const;
    QString address() const;
    QString postalcode() const;
    QString city() const;
    QString abbrv() const;
    QString email() const;
    QString phone() const;
    QString region() const;
    
    void setId(QString id);
    void setTitle(QString title);
    void setInitials(QString initials);
    void setPrefixes(QString prefixes);
    void setName(QString name);
    void setAddress(QString addres);
    void setpostalcode(QString code);
    void setCity(QString city);
    void setAbbrv(QString abbrv);
    void setEmail(QString email);
    void setPhone(QString phone);
    void setRegion(QString region);
    
private:
    QExplicitlySharedDataPointer<BreederData> data;
};


QDataStream & operator<< (QDataStream& stream, const Breeder& b);
QDataStream & operator>> (QDataStream& stream, Breeder& b);

Q_DECLARE_METATYPE(Breeder);

#endif // BREEDER_H
