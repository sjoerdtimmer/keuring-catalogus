#include "datastructures/show.h"
#include "datastructures/sheep.h"
#include "datastructures/breeder.h"

#include "database.h"

#include <QDebug>

class ShowData : public QSharedData
{
public:
    QDate date;
    QString name;
    QHash<QString,Sheep> sheep;
    QHash<QString,Breeder> breeders;
};

Show::Show() : data(new ShowData)
{
//    qDebug() << "show created";
    setName(Database::instance()->nextAvailableName());
    setDate(QDate::currentDate());
}

Show::Show(const Show &rhs) : data(rhs.data)
{
//    qDebug() << QString("show %1 cloned, ref counter now %2").arg(data->name).arg(data.constData()->ref);
    this->listitem = rhs.listitem;
    setName(data->name);
}

Show &Show::operator=(const Show &rhs)
{
    if (this != &rhs)
    {
        this->listitem = rhs.listitem;
        data.operator=(rhs.data);
    }
//    qDebug() << "show assigned";
//    qDebug() << QString("show %1 assigned, ref counter now %2").arg(data->name).arg(data.constData()->ref);
    this->setName(rhs.name());
    return *this;
}

Show::~Show()
{
//    qDebug() << QString("destructing show %1 with ref counter %2").arg(name()).arg(data.constData()->ref);
}


void Show::setName(QString name) {
//    qDebug() << QString("show %1 renamed to %2").arg(data->name).arg(name);
    data->name = name;
    if (this->listitem)
    {
        this->listitem->setText(name + QString(" - ") + this->date().toString(QString("dddd d MMMM yyyy")));
    }
}
void Show::setDate(QDate date) { 
    data->date = date; 
}
void Show::setSheep(QHash<QString,Sheep> sheep) { data->sheep = sheep; }
void Show::setBreeders(QHash<QString,Breeder> breeders) { data->breeders = breeders; }


QString Show::name() const { return data->name; }
QDate Show::date() const { return data->date; }
QHash<QString,Sheep> Show::sheep() const { return data->sheep; }
QHash<QString,Breeder> Show::breeders() const { return data->breeders; }


ShowListItem::ShowListItem(Show& s)
{
//    qDebug() << QString("ListItem created for %1").arg(s.name());
    // set the listitem of the show to this
    // all copy and assignment operators copy this pointer
    s.listitem = this;
    // save a shallow reference
    // as long as this is around qimplicitsharing will not delete the show
    show = s;
    /*connect(show, &Show::dateChanged, [=](){
        QString fmt = QLocale().dateFormat(QLocale::LongFormat);
    //    qDebug() << "the locale appears to be " << QLocale().languageToString(QLocale().language());
        setText(s.name() + QString(" - ") + s.date().toString(fmt));
    });*/
    // set visual appearance equal to show name
    setText(s.name());
    QString fmt = QLocale().dateFormat(QLocale::LongFormat);
//    qDebug() << "the locale appears to be " << QLocale().languageToString(QLocale().language());
    setText(s.name() + QString(" - ") + s.date().toString(fmt));
}

ShowListItem::~ShowListItem()
{
//    qDebug() << QString("ListItem for %1 destroyed").arg(show.name());
    show.listitem = nullptr;
    show.~Show();
}

QDataStream & operator<< (QDataStream& stream, const Show& s) 
{
    stream << s.name() << s.date() << s.sheep() << s.breeders();
    return stream;
}
QDataStream & operator>> (QDataStream& stream, Show& s) 
{
    QString name;
    QDate date;
    QHash<QString, Sheep> sheep;
    QHash<QString, Breeder> breeders;
    stream >> name >> date >> sheep >> breeders;
    s.setName(name);
    s.setDate(date);
    // this looks expensive but sheep/breeders and QHashes use implicit sharing and shallow copies so no data is unnecessarily copied
    s.setSheep(sheep);
    s.setBreeders(breeders);
    
    return stream;
}

// make Show comparable
bool ShowListItem::operator < (const QListWidgetItem &other) const
{
    ShowListItem * o = (ShowListItem *)&other;
    return this->show.date() < o->show.date();
}

void::ShowListItem::updateText()
{
    QString fmt = QLocale().dateFormat(QLocale::LongFormat);
//    qDebug() << "the locale appears to be " << QLocale().languageToString(QLocale().language());
    setText(show.name() + QString(" - ") + show.date().toString(fmt));
}

bool Show::operator == (const QListWidgetItem &other) const
{
    Show * o = (Show *)&other;
    return this->data == o->data;
}
bool Show::operator == (const Show &other) const
{
    return this->data == other.data;
}

