#ifndef SHEEP_H
#define SHEEP_H

#include <QDate>
#include <QExplicitlySharedDataPointer>
#include "breeder.h"

class SheepData;

class Sheep
{
public:
    enum Gender {Ewe, Ram};
    enum Rank {RankNone, RankA, RankB, RankC};
    
public:
    Sheep();
    Sheep(const Sheep &);
    Sheep &operator=(const Sheep &);
    ~Sheep();
    Sheep clone();
    
    static QString shortName(QString s);
    
    void reset();
    
    QString stn() const;
    QString uln() const;
    QString owner() const;
    QString breeder() const;
    QDate dateOfBirth() const;
    QString father() const;
    QString mother() const;
    Sheep::Gender gender() const;
    bool doesParticipate() const;
    bool doesCompanyThree() const;
    bool doesPairOfRam() const;
    bool doesTripleOfRam() const;
    QString childPresentation() const;
    int subcategory() const;
    int order() const;
    Sheep::Rank rank() const;
    int place() const;
    QString comment() const;
    QString tailindex() const;
    
    void setStn(QString stn);
    void setUln(QString uln);
    void setOwner(QString breeder);
    void setBreeder(QString breeder);
    void setDateOfBirth(QDate date);
    void setFather(QString father);
    void setMother(QString mother);
    void setGender(Sheep::Gender gender);
    void setDoesParticipate(bool state);
    void setDoesCompanyThree(bool state);
    void setDoesPairOfRam(bool state);
    void setDoesTripleOfRam(bool state);
    void setChildPresentation(QString childName);
    void setSubcategory(int subcategory);
    void setOrder(int order);
    void setRank(Sheep::Rank rank);
    void setPlace(int place);
    void setComment(QString comment);
    void setTailindex(QString);
    
private:
    QExplicitlySharedDataPointer<SheepData> data;
};


inline uint qHash(const Sheep &s) {
    return qHash(s.stn(), 0xa03f); // arbitrary value
}
inline bool operator==(const Sheep& lhs, const Sheep& rhs) {
    return lhs.stn() == rhs.stn();
}


Q_DECLARE_METATYPE(Sheep);

QDataStream & operator<< (QDataStream& stream, const Sheep::Gender& g);
QDataStream & operator>> (QDataStream& stream, Sheep::Gender& g);
QDataStream & operator<< (QDataStream& stream, const Sheep::Rank& r);
QDataStream & operator>> (QDataStream&stream, Sheep::Rank& r);
QDataStream & operator<< (QDataStream& stream, const Sheep& s);
QDataStream & operator>> (QDataStream& stream, Sheep& s);

#endif // SHEEP_H
