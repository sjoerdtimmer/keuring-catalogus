#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "datastructures/show.h"
#include "database.h"
#include "wizards/editshowwizard.h"
#include "wizards/registrationwizard.h"
#include "wizards/categorywizard.h"
#include "wizards/resultswizard.h"
#include "wizards/invitationswizard.h"
#include "wizards/bookletwizard.h"
#include "wizards/inventoryexportwizard.h"

// Qt stuff
#include <QLabel>
#include <QListView>
#include <QStringList>
#include <QStringListModel>
#include <QPushButton>
#include <QDateEdit>
#include <QLineEdit>
#include <QSortFilterProxyModel>
#include <QMessageBox>
#include <QtDebug>
#include <QDataStream>

// standard stuff
#include <stdio.h>
#include <iostream>



using std::cout;
using std::cerr;
using std::endl;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->list_Shows->setSortingEnabled(true);

    connect(ui->list_Shows->model(), &QAbstractItemModel::rowsInserted, this, &MainWindow::on_show_added);
    connect(ui->list_Shows, &QListWidget::currentItemChanged, this, &MainWindow::on_show_selectionChanged);

    connect(Database::instance(),&Database::showRead, [=](Show& show)
    {
        qDebug() << "showRead trigger received";
        ui->list_Shows->addItem(new ShowListItem(show));
    });
    Database::instance()->loadShows();


}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_add_show_clicked()
{
    // a pointer is required since even the shareddatapointer get destroyed at the end of scope
    // and QListWidget doesn't undertand sharedpointers
    Show show;
    qDebug() << "new show created: " << show.name();
    Database::instance()->shows.append(show);
    Database::instance()->saveShows();
    ui->list_Shows->addItem(new ShowListItem(show));
}


void MainWindow::on_show_added()
{
    qDebug() << "sorting shows again";
//    qDebug() << "the list containts " << ui->list_Shows->count() << "items";
    ui->list_Shows->sortItems(Qt::DescendingOrder);
}

void MainWindow::on_show_selectionChanged(const QListWidgetItem *current, const QListWidgetItem *previous)
{
    // disable gui when nothing is selected
    ui->pushButton_export_invitations->setEnabled(!!current);
    ui->pushButton_export_categories->setEnabled(!!current);
    ui->pushButton_export_stable_inventory->setEnabled(!!current);
//    ui->pushButton_export_results->setEnabled(!!current);
    
    ui->pushButton_manage_registrations->setEnabled(!!current);
    ui->pushButton_manage_catagories->setEnabled(!!current);
    ui->pushButton_manage_results->setEnabled(!!current);
    
    ui->pushButton_remove_show->setEnabled(!!current);
    ui->pushButton_edit_show->setEnabled(!!current);
}


void MainWindow::on_pushButton_quit_clicked()
{
    close();
}


void MainWindow::on_pushButton_remove_show_clicked()
{
    ShowListItem * current = (ShowListItem*) ui->list_Shows->currentItem();
    QMessageBox msgBox;
    msgBox.setText(tr("Weet je het zeker?"));
    msgBox.setInformativeText(tr("Alle informatie die geassocieerd is met de keuring '%1' zal permanent en onomkeerbaar verwijderd worden. ").arg(current->show.name()));
    msgBox.setStandardButtons(QMessageBox::Cancel | QMessageBox::Yes);
    msgBox.setDefaultButton(QMessageBox::Cancel);

    if(msgBox.exec() == QMessageBox::Yes)
    {
        {
            QFile(Database::sanitizeShowFileName(current->show)).remove();
//            qDebug() << "file removed";
        }
        {
            Database::instance()->shows.removeAll(current->show);
//            qDebug() << "show instances in database removed";
        }
        {
//            ui->list_Shows->removeItemWidget(current);
//            qDebug() << "item widget removed";
        }
        
        {
            // causes segfault because removeItemWidget already deleted it
            delete current;
//            qDebug() << "show list item deleted";
        }
        
    }
}

void MainWindow::on_pushButton_manage_registrations_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    RegistrationWizard d(current->show);
    d.exec();
}

void MainWindow::on_pushButton_clicked()
{
    Database::instance()->saveShows();
}

void MainWindow::on_pushButton_export_invitations_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    InvitationsWizard d(current->show);
    d.exec();
}

void MainWindow::on_pushButton_manage_catagories_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    CategoryWizard d(current->show);
    d.exec();
}

void MainWindow::on_pushButton_export_categories_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    BookletWizard d(current->show);
    d.exec();
}

void MainWindow::on_pushButton_edit_show_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    EditShowWizard d(current->show);
    d.exec();
    current->updateText();
}

void MainWindow::on_pushButton_manage_results_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    ResultsWizard d(current->show);
    d.exec();
}

void MainWindow::on_pushButton_export_stable_inventory_clicked()
{
    ShowListItem * current = (ShowListItem *) ui->list_Shows->currentItem();
    InventoryExportWizard d(current->show);
    d.exec();
}
