#-------------------------------------------------
#
# Project created by QtCreator 2017-10-29T09:36:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ClunForestCatalogus
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    mainwindow.cpp \
    database.cpp \
    wizards/editshowwizard.cpp \
    datastructures/sheep.cpp \
    datastructures/breeder.cpp \
    datastructures/show.cpp \
    exceptions.cpp \
    wizards/registrationwizard.cpp \
    wizards/wizardconfirmationpage.cpp \
    wizards/categorywizard.cpp \
    wizards/categorywizardpage.cpp \
    wizards/resultswizard.cpp \
    wizards/resultswizardpage.cpp \
    wizards/invitationswizard.cpp \
    WordProcessingCompiler.cpp \
    WordProcessingMerger.cpp \
    wizards/bookletwizard.cpp \
    wizards/commentdialog.cpp \
    wizards/inventoryexportwizard.cpp

HEADERS += \
    mainwindow.h \
    database.h \
    wizards/editshowwizard.h \
    datastructures/sheep.h \
    datastructures/breeder.h \
    datastructures/show.h \
    exceptions.h \
    wizards/registrationwizard.h \
    wizards/wizardconfirmationpage.h \
    wizards/categorywizard.h \
    wizards/categorywizardpage.h \
    wizards/resultswizard.h \
    wizards/resultswizardpage.h \
    wizards/invitationswizard.h \
    wizards/bookletwizard.h \
    wizards/commentdialog.h \
    wizards/inventoryexportwizard.h

FORMS += \
    mainwindow.ui \
    wizards/editshowwizard.ui \
    wizards/registrationwizard.ui \
    wizards/wizardconfirmationpage.ui \
    wizards/categorywizard.ui \
    wizards/categorywizardpage.ui \
    wizards/resultswizard.ui \
    wizards/resultswizardpage.ui \
    wizards/invitationswizard.ui \
    wizards/bookletwizard.ui \
    wizards/commentdialog.ui \
    wizards/inventoryexportwizard.ui

RC_FILE = myapp.rc

RESOURCES += \
    icons.qrc \
    templates.qrc

#win32: LIBS += -L$$PWD/../../../../../../DocxFactory/lib/ -lDocxFactory
#INCLUDEPATH += $$PWD/../../../../../../DocxFactory/include
#DEPENDPATH += $$PWD/../../../../../../DocxFactory/include
win32: LIBS += -LC:/DocxFactory/lib/ -lDocxFactory
INCLUDEPATH += C:/DocxFactory/include
DEPENDPATH += C:/DocxFactory/include

