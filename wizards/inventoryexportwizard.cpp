#include "inventoryexportwizard.h"
#include "ui_inventoryexportwizard.h"

#include "WordProcessingCompiler.h"
#include "WordProcessingMerger.h"
#include <exception>
#include <iostream>
#include <stdexcept>

#include <QFileDialog>
#include <QDebug>
#include <QTemporaryDir>


using namespace DocxFactory;
using namespace  std;

InventoryExportWizard::InventoryExportWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::InventoryExportWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}

InventoryExportWizard::~InventoryExportWizard()
{
    delete ui;
}

InventoryExportWizard::InventoryExportWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::InventoryExportWizard),
    m_show(show)
{
    ui->setupUi(this);
    this->showMaximized();
    this->setButtonText(InventoryExportWizard::FinishButton, tr("Opslaan"));
    this->setButtonText(InventoryExportWizard::CancelButton, tr("Sluiten"));

    //ui->dateEdit->setDate(QDate::currentDate());
    
    // this will disconnect ALL signals from the finish button
    this->disconnect(button(InventoryExportWizard::FinishButton), nullptr, this, nullptr);
    this->connect(button(InventoryExportWizard::FinishButton), &QPushButton::clicked, [=]()
    {
        makeDocument();
    });
}


void InventoryExportWizard::makeDocument() {
    //if (this->ui->dateEdit->date())
    try
    {
        qDebug() << "getting word processing instances";
        WordProcessingMerger& l_merger = WordProcessingMerger::getInstance();
        WordProcessingCompiler& l_compiler = WordProcessingCompiler::getInstance();

        // create a temporary dir for compiling and executing the template
        qDebug() << "getting a suitable temp dir";
        QTemporaryDir tempdir;
        tempdir.setAutoRemove(false);
        if (!tempdir.isValid())
        {
            throw logic_error("failed to create a writable temp dir");
        }

        std::string inputfile        = ui->lineEdit_template->text().toStdString();
        std::string compiledtemplate = QString("%1/%2").arg(tempdir.path()).arg("template.dfw").toStdString();
        std::string outputfile       = ui->lineEdit_output->text().toStdString();

        qDebug() << "compiling template to " << QString("%1/%2").arg(tempdir.path()).arg("template.dfw");
        // compile input docx to dfw template
        l_compiler.compile(inputfile, compiledtemplate);

        qDebug() << "loading compiled template for merging";
        // load dfw for merging
        l_merger.load(compiledtemplate);

        // create a locale
        QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);
        
        qDebug() << "attempting to print list of locales";
        QList<QLocale> allLocales = QLocale::matchingLocales(
                    QLocale::AnyLanguage,
                    QLocale::AnyScript,
                    QLocale::AnyCountry);
        QSet<QString> iso639Languages;
        
        for(const QLocale &locale : allLocales) {
            iso639Languages.insert(QLocale::languageToString(locale.language()));
        }
        
        qDebug() << "all locales: " << iso639Languages;
        
//        QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);
//        QLocale locale = QLocale();

        // create list of unique breeders:
        QMultiHash<QString, Sheep> sheepbyowner;
        for (Sheep s : m_show.sheep())
        {
            sheepbyowner.insert(s.owner(), s);
        }

        // make a set of unique owner names (sheepbyowner.keys() contains duplicates)
        QSet<QString> owners;
        for(QString owner : sheepbyowner.keys())
        {
            owners.insert(owner);
        }

        // for each owner print one document
        for (QString ownerId : owners)
        {
            Breeder owner = m_show.breeders().value(ownerId);
            QString fullName = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
        
            l_merger.setClipboardValue("voorblad", "naamfokker", fullName.toStdString());
            l_merger.setClipboardValue("voorblad", "adresfokker", owner.address().toStdString());
            l_merger.setClipboardValue("voorblad", "postcodefokker", owner.postalcode().toStdString());
            l_merger.setClipboardValue("voorblad", "woonplaatsfokker", owner.city().toStdString());
            l_merger.setClipboardValue("voorblad", "emailfokker", owner.email().toStdString());
            l_merger.setClipboardValue("voorblad", "fokletters", owner.abbrv().toStdString());
            l_merger.setClipboardValue("voorblad", "ubnfokker", owner.id().toStdString());
            l_merger.setClipboardValue("voorblad", "telefoonfokker", owner.phone().toStdString());
            l_merger.setClipboardValue("voorblad", "inspectiegebied", owner.region().toStdString());
            l_merger.setClipboardValue("voorblad", "datuminspectie", locale.toString(ui->dateEdit->date(), "d MMMM yyyy").toStdString());
            l_merger.paste("voorblad");
            
            //l_merger.paste("schapenlijst");
            
            // sort sheep by date of birth
            QMultiMap<QDate, Sheep> sortmap;
            for (Sheep s : sheepbyowner.values(ownerId)) {
                sortmap.insert(s.dateOfBirth(), s);
            }
            QList<Sheep> m_sheep = sortmap.values();
            qDebug() << "making a list of " << m_sheep.size() << "sheep";
            
            
            for (int i=0;i<2;++i) {
                l_merger.setClipboardValue("schapenlijst", "naamfokker", fullName.toStdString());
                l_merger.setClipboardValue("schapenlijst", "adresfokker", owner.address().toStdString());
                l_merger.setClipboardValue("schapenlijst", "postcodefokker", owner.postalcode().toStdString());
                l_merger.setClipboardValue("schapenlijst", "woonplaatsfokker", owner.city().toStdString());
                l_merger.setClipboardValue("schapenlijst", "emailfokker", owner.email().toStdString());
                l_merger.setClipboardValue("schapenlijst", "fokletters", owner.abbrv().toStdString());
                l_merger.setClipboardValue("schapenlijst", "ubnfokker", owner.id().toStdString());
                l_merger.setClipboardValue("schapenlijst", "telefoonfokker", owner.phone().toStdString());
                l_merger.setClipboardValue("schapenlijst", "inspectiegebied", owner.region().toStdString());
                l_merger.setClipboardValue("schapenlijst", "datuminspectie", locale.toString(ui->dateEdit->date(), "d MMMM yyyy").toStdString());
                
                l_merger.paste("schapenlijst");
                
                for (Sheep s : m_sheep)
                {
                    l_merger.setClipboardValue("schaap", "uln", s.stn().toStdString());
                    l_merger.setClipboardValue("schaap", "geslacht", s.gender()==Sheep::Ewe?"Ooi":"Ram");
                    l_merger.setClipboardValue("schaap", "staartindex", s.tailindex().toStdString());
                    l_merger.setClipboardValue("schaap", "dob", locale.toString(s.dateOfBirth(), "dd/MM/yyyy").toStdString());
                    l_merger.paste("schaap");
                }
            }
            
        }
        // saving output
        l_merger.save(outputfile);

        ui->label_error->setText("✔");
        ui->label_error->setStyleSheet("QLabel { color : green; }");
        ui->label_errordetail->setText("Succesvol opgeslagen!");
    }
    catch (const exception& p_exception)
    {
        qDebug() << p_exception.what();
        ui->label_error->setText("✖");
        ui->label_error->setStyleSheet("QLabel { color : red; }");
        ui->label_errordetail->setText(p_exception.what());
    }
}

void InventoryExportWizard::on_pushButton_browsetemplate_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Template openen voor inspectielijsten..."), tr("inspectielijsten-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_template->setText(fileName);
}

void InventoryExportWizard::on_pushButton_browseoutput_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Output bestand voor inspectielijsten..."), tr("inspectielijsten.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_output->setText(fileName);
}

void InventoryExportWizard::on_pushButton_exporttemplate_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Template voor inspectielijsten opslaan..."), tr("inspectielijsten-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    if (!fileName.endsWith(".docx", Qt::CaseInsensitive))
    {
        fileName.append(".docx");
    }
    if (!QFile::copy(":/templates/inspectie-template.docx", fileName))
    {
        qWarning() << tr("failed to write template to file '%1'").arg(fileName);
    }
    else 
    {
        ui->lineEdit_template->setText(fileName);
    }
}
