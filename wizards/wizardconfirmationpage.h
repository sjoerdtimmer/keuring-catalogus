#ifndef WIZARDCONFIRMATIONPAGE_H
#define WIZARDCONFIRMATIONPAGE_H

#include <QWizardPage>
#include "exceptions.h"

namespace Ui {
class WizardConfirmationPage;
}

class WizardConfirmationPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit WizardConfirmationPage(QWidget *parent = nullptr);
    ~WizardConfirmationPage();

//    void setStatus(bool acceptable, QString msg);
    void setStatus(QList<CustomException> msgs);

private:
    Ui::WizardConfirmationPage *ui;
    bool allisok = false;
    bool isComplete() const;
};

#endif // WIZARDCONFIRMATIONPAGE_H
