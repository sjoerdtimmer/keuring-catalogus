#ifndef INVENTORYEXPORTWIZARD_H
#define INVENTORYEXPORTWIZARD_H

#include "WordProcessingCompiler.h"
#include "WordProcessingMerger.h"

#include "datastructures/show.h"

#include <QWizard>


namespace Ui {
class InventoryExportWizard;
}

class InventoryExportWizard : public QWizard
{
    Q_OBJECT
    
public:
    explicit InventoryExportWizard(QWidget *parent = nullptr);
    explicit InventoryExportWizard(Show show, QWidget *parent = nullptr);
    ~InventoryExportWizard();
    
private slots:
    void on_pushButton_browsetemplate_clicked();
    
    void on_pushButton_browseoutput_clicked();
    
    void on_pushButton_exporttemplate_clicked();
    
private:
    Ui::InventoryExportWizard *ui;
    Show m_show;
    void makeDocument();
};

#endif // INVENTORYEXPORTWIZARD_H
