#ifndef EDITSHOWWIZARD_H
#define EDITSHOWWIZARD_H

#include "datastructures/show.h"
#include "exceptions.h"

#include <QWizard>
#include <QTextStream>

namespace Ui {
class EditShowWizard;
}

class EditShowWizard : public QWizard
{
    Q_OBJECT
    
public:
    explicit EditShowWizard(QWidget *parent = 0);
    explicit EditShowWizard(Show, QWidget *parent=0);
    ~EditShowWizard();

private:
    void loadBreedersFromFile(QString filename, QList<CustomException>& exceptions);
    void loadSheepFromFile(QString filename, QList<CustomException>& exceptions);
    
    
private slots:
    void on_pushButton_browseSheepFile_clicked();
    
    void on_pushButton_browseBreedersFile_clicked();
    
    void on_comboBox_copyFrom_activated(const QString &arg1);
    
    void on_EditShowWizard_accepted();
    
    void on_EditShowWizard_currentIdChanged(int id);
    
private:
    Ui::EditShowWizard *ui;
    Show m_show;
    QHash<QString,Sheep> newsheep;
    QHash<QString,Breeder> newbreeders;
    QString normalizeDateString(QString in);
};

#endif // EDITSHOWWIZARD_H
