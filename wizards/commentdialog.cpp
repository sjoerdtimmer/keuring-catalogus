#include "commentdialog.h"
#include "ui_commentdialog.h"

#include <QDebug>

CommentDialog::CommentDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::CommentDialog)
{
    ui->setupUi(this);
}

CommentDialog::~CommentDialog()
{
    delete ui;
}

void CommentDialog::setSheepName(QString name) {
    this->setWindowTitle(tr("Update opmerking voor %1").arg(name));
}

void CommentDialog::setComment(QString txt) {
    ui->lineEdit->setText(txt);
}

QString CommentDialog::getComment() const {
    qDebug() << "new comment: " << ui->lineEdit->text();
    return ui->lineEdit->text();
}
