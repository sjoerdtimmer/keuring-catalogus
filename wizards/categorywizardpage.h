#ifndef CATEGORYWIZARDPAGE_H
#define CATEGORYWIZARDPAGE_H

#include "datastructures/show.h"

#include <QWizardPage>



namespace Ui {
class CategoryWizardPage;
}

class CategoryWizardPage : public QWizardPage
{
    Q_OBJECT
    
public:
    explicit CategoryWizardPage(QWidget *parent = nullptr);
    ~CategoryWizardPage();
    void addSheep(Sheep);
    void saveSheep(Show);
    int getUnassigned();
    void setMaxPerCategory(int max);
    void setShow(Show);

signals:
    void maxPerCategoryEdited(int max);

    
private:
    Ui::CategoryWizardPage *ui;
    QList<Sheep> m_sheep;
    QList<QListWidget*> lists;
    Show m_show;
    
    void addList(const QString &name);
    void removeLastList();
    
    void ensureAtLeastNGroups(int n);
    
public slots:
    void adjustNumLists();
    void autoDistribution(bool);
    void initDistribution();
    

private slots:
    void on_spinBox_editingFinished();
    void on_pushButton_auto_clicked();
    void on_pushButton_clicked();
};

#endif // CATEGORYWIZARDPAGE_H
