#ifndef BOOKLETWIZARD_H
#define BOOKLETWIZARD_H

#include "datastructures/show.h"

#include "WordProcessingCompiler.h"
#include "WordProcessingMerger.h"

#include <QWizard>
#include <QThread>

using namespace DocxFactory;



class BookletThread : public QThread {
    Q_OBJECT
    
    Show m_show;
    QString inputfile;
    QString outputfile;
public:
    BookletThread(QObject *parent, Show show, QString inputfile, QString outputfile);
    void run() override;
   
private:
    void makeBooklet();
    QList<Sheep> filterSheep(int minyear, int maxyear, Sheep::Gender gender);
    void makeParticipantsPage(WordProcessingMerger& l_merger);
    void makeCategory(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeCategoryPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeSpecialPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeChildPresentationPage(WordProcessingMerger& l_merger, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    
signals:
    void done();
    void error(QString msg);
};






namespace Ui {
class BookletWizard;
}

class BookletWizard : public QWizard
{
    Q_OBJECT

public:
    explicit BookletWizard(QWidget *parent = nullptr);
    explicit BookletWizard(Show show, QWidget *parent = nullptr);
    ~BookletWizard();
    
    
private slots:
    void on_pushButton_browsetemplate_clicked();

    void on_pushButton_browseoutput_clicked();

    void on_pushButton_exporttemplate_clicked();

private:
    Ui::BookletWizard *ui;
    Show m_show;

    void makeBooklet();
    QList<Sheep> filterSheep(int minyear, int maxyear, Sheep::Gender);
    void makeCategory(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeCategoryPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeSpecialPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep);
    void makeChildPresentationPage(WordProcessingMerger &l_merger, int *catcounter, int *sheepcounter, QList<Sheep> sheep);
    void makeParticipantsPage(WordProcessingMerger &l_merger);
};

#endif // BOOKLETWIZARD_H
