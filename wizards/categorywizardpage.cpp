#include "categorywizardpage.h"
#include "ui_categorywizardpage.h"

#include <QtMath>
#include <QDebug>
#include <QMultiMap>

#include <algorithm>
#include <iostream>

using std::cout;
using std::endl;

CategoryWizardPage::CategoryWizardPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::CategoryWizardPage)
{
    ui->setupUi(this);
}

CategoryWizardPage::~CategoryWizardPage()
{
    delete ui;
}


void CategoryWizardPage::setShow(Show s)
{
    m_show = s;
}

// add, if needed, extra lists until there are at least N lists for groups
// also creates 0th list if needed, and adds an extra for dragging and dropping
void CategoryWizardPage::ensureAtLeastNGroups(int n)
{
    if (lists.isEmpty())
    {
        addList(tr("niet ingedeeld"));
        connect(lists.last()->model(), &QAbstractItemModel::rowsRemoved, this, &CategoryWizardPage::adjustNumLists);
    }
    // N groups, means N+1 lists
    while (lists.size() -1 < n+1)
    {
        addList(tr("groep %1").arg(lists.size()));
        connect(lists.last()->model(), &QAbstractItemModel::rowsRemoved, this, &CategoryWizardPage::adjustNumLists);
    }
}

// add one list at the end
void CategoryWizardPage::addList(const QString &title)
{
    QListWidget * listwidget = new QListWidget(this);
    listwidget->setDragDropMode(QAbstractItemView::DragDrop);
    listwidget->setSelectionMode(QAbstractItemView::SingleSelection);
    listwidget->setDefaultDropAction(Qt::MoveAction);
    listwidget->setAlternatingRowColors(true);
    ui->layoutForLists->addWidget(listwidget);
    ui->layoutForTitles->addWidget(new QLabel(title));
    lists.append(listwidget);
}

// remove the last list
void CategoryWizardPage::removeLastList()
{
    // remove from layout:
    QListWidget * listwidget = lists.last();
    ui->layoutForLists->removeWidget(listwidget);
    // also remove label:
    QWidget * label = ui->layoutForTitles->itemAt(ui->layoutForTitles->count()-1)->widget();
    ui->layoutForTitles->removeWidget(label);
    // remove from list of listwidgets:
    lists.removeAll(listwidget);
    // delete the objects:
    delete label;
    delete listwidget;
}



void CategoryWizardPage::autoDistribution(bool distributeBreeders)
{
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

    QMultiMap<QString, Sheep> sortmap;
    for (Sheep s : m_sheep) {
        if (distributeBreeders)
        {
            sortmap.insert(s.owner() + s.dateOfBirth().toString("yyyyMMdd"), s);
        }
        else 
        {
            sortmap.insert(s.dateOfBirth().toString("yyyyMMdd"), s);
        }
    }
    m_sheep = sortmap.values();

    // decide on number of groups:
    int numgroups = qCeil((double)m_sheep.size() / (double)ui->spinBox->value());
    int sheepPerGroup = qCeil((double)m_sheep.size()/(double)numgroups);

    // make a hash that puts every sheep in a target group:
    QHash<QString, int> targetgroups;
    
    if (distributeBreeders)
    {
        int group = 1;
        for (Sheep s : m_sheep)
        {
            targetgroups.insert(s.stn(), group++);
            if (group > numgroups) group = 1;
        }            
    }
    else 
    {
        for (int i=0;i<m_sheep.size();++i)
        {
            targetgroups.insert(m_sheep[i].stn(), 1+qFloor((double)i/(double)sheepPerGroup));
        }
            
    }


    
    
    // print the list for debugging:        
//    cout << "sorted list of sheep:" << endl;
//    for (Sheep * s : m_sheep)
//    {
//        cout << s->id << ": " << s->dateOfBirth.toString("d/M/yyyy") << ": " << targetgroups.value(s->id) << endl;
//    }
    
    // make sure there are enough lists:
    ensureAtLeastNGroups(numgroups);
    
    // clear current distribution:
    for (QListWidget * widget : lists)
    {
        widget->clear();
    }
    
    // add sheep according to target groups:
    for (Sheep s : m_sheep)
    {
        QListWidget * t = lists.at(targetgroups.value(s.stn(), 0));
//        QListWidgetItem * item = new QListWidgetItem(tr("%1").arg(s->id), t);
        Breeder owner = m_show.breeders().value(s.owner());
        QListWidgetItem * item = new QListWidgetItem(tr("%1\n%2 %3 %4\n%5")
                                                     .arg(Sheep::shortName(s.stn()))
                                                     .arg(owner.initials())
                                                     .arg(owner.prefixes())
                                                     .arg(owner.name())
                                                     .arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy")),  t);
//        item->setToolTip(tr("%1\n%2\n%3").arg(s->id).arg(s->ownerName).arg(s->dateOfBirth.toString()));
//        QListWidgetItem * item = new QListWidgetItem(tr("%1 (%2)").arg(s->id).arg(s->dateOfBirth.toString()), t);
        item->setData(Qt::UserRole, QVariant::fromValue(s.stn()));
    }
    
    // remove any extra lists that may not be needed anymore:
    adjustNumLists();    
    
    // connect signals:
    for (QListWidget * list : lists)
    {
        connect(list->model(), &QAbstractItemModel::rowsRemoved, this, &CategoryWizardPage::adjustNumLists);
    }
}



/* initDistribution uses the categories from the database */
void CategoryWizardPage::initDistribution()
{
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

    // clear current distribution:
    for (QListWidget * widget : lists)
    {
        widget->clear();
    }
    
    // sort the sheep by previously saved order, if the overall list is sorted, so must each subcategory be:
    // ugly sort
    QMultiMap<int, Sheep> sortmap;
    for (Sheep& s : m_sheep) {
        sortmap.insert(s.order(), s);
    }
    m_sheep.clear();
    m_sheep.append(sortmap.values());

    
    // count the categories:
    int num_categories = 0;
    for (Sheep& s : m_sheep)
    {
        num_categories = std::max(num_categories, s.subcategory());
    }
    
    // create the list widgets:
    ensureAtLeastNGroups(num_categories);
    
    // distribute the sheep:
    for (Sheep& s : m_sheep)
    {
        QListWidget * t = lists.at(s.subcategory());
//        QListWidgetItem * item = new QListWidgetItem(tr("%1").arg(s->id), t);
        Breeder owner = m_show.breeders().value(s.owner());
        QListWidgetItem * item = new QListWidgetItem(tr("%1\n%2 %3 %4\n%5")
                                                     .arg(Sheep::shortName(s.stn()))
                                                     .arg(owner.initials())
                                                     .arg(owner.prefixes())
                                                     .arg(owner.name())
                                                     .arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy")), t);
        item->setData(Qt::UserRole, QVariant::fromValue(s.stn()));
    }    
    
    // remove any extra lists that may not be needed anymore:
    adjustNumLists();
    
    // connect signals:
    for (QListWidget * list : lists)
    {
        connect(list->model(), &QAbstractItemModel::rowsRemoved, this, &CategoryWizardPage::adjustNumLists);
    }
}


void CategoryWizardPage::setMaxPerCategory(int max)
{
    ui->spinBox->setValue(max);
}


void CategoryWizardPage::adjustNumLists()
{
    // remove lists if needed:
    {
        while (lists.size() > 2)
        {
            QListWidget * w1 = lists.at(lists.size()-1);
            QListWidget * w2 = lists.at(lists.size()-2);
            
            if (w1->count()==0 && w2->count()==0)
            {
                removeLastList();
            }
            else
            {
                break;
            }
        }    
    }
    
    // append list if needed
    if (lists.last()->count() > 0)
    {
        addList(tr("groep %1").arg(lists.count()));
        connect(lists.last()->model(), &QAbstractItemModel::rowsRemoved, this, &CategoryWizardPage::adjustNumLists);
    }
}

int CategoryWizardPage::getUnassigned()
{
    return lists.at(0)->count();
}



void CategoryWizardPage::addSheep(Sheep s)
{
    m_sheep.append(s);
}

void CategoryWizardPage::saveSheep(Show show)
{
    for (int j=0 ; j<lists.size() ; j++)
    {
        QListWidget * list  = lists.at(j);
        for (int i=0 ; i<list->count() ; i++)
        {
            QVariant var = list->item(i)->data(Qt::UserRole);
            if (show.sheep().contains(var.toString()))
            {
                Sheep s = show.sheep().value(var.toString());
                s.setSubcategory(j);
                s.setOrder(i);
            }
            else
            {
                qDebug() << "error: an item from the list does not really exist?";
            }
        }
    }
}



void CategoryWizardPage::on_spinBox_editingFinished()
{
    emit maxPerCategoryEdited(ui->spinBox->value());
}

void CategoryWizardPage::on_pushButton_auto_clicked()
{
    autoDistribution(false);
}

void CategoryWizardPage::on_pushButton_clicked()
{
    autoDistribution(true);
}
