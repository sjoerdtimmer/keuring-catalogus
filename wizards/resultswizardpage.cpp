#include "resultswizardpage.h"
#include "commentdialog.h"
#include "ui_resultswizardpage.h"

#include <QDebug>


ResultsWizardPage::ResultsWizardPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::ResultsWizardPage)
{
    ui->setupUi(this);
    
    // setup drag-drop behaviour of our lists:
    QList<QListWidget *> lists;
    lists.append(ui->listWidget_none);
    lists.append(ui->listWidget_A);
    lists.append(ui->listWidget_B);
    lists.append(ui->listWidget_C);
    for(QListWidget* list : lists) {
        list->setDragDropMode(QAbstractItemView::DragDrop);
//        list->setSelectionMode(QAbstractItemView::SingleSelection);
        list->setDefaultDropAction(Qt::MoveAction);
        list->setAlternatingRowColors(true);
        connect(list, &QListWidget::itemDoubleClicked, this, &ResultsWizardPage::on_listWidget_itemDoubleClicked);
    }
    
    
    
}

ResultsWizardPage::~ResultsWizardPage()
{
    delete ui;
}


void ResultsWizardPage::addSheep(Sheep s) {
    m_sheep.append(s);
}

// distribute sheep over lists according to current ranking:
void ResultsWizardPage::setShow(Show show) {
    m_show = show;
    // make sorted list of sheep sorted by place:
    QMultiMap<int, Sheep> sortmap;
    for (Sheep s : m_sheep) {
        sortmap.insert(s.place(), s);
    }
    m_sheep = sortmap.values();
    
    
    
    for (Sheep& s : m_sheep) {
        QListWidget * lw = ui->listWidget_none;
        if (s.rank()==Sheep::RankA) lw = ui->listWidget_A;
        if (s.rank()==Sheep::RankB) lw = ui->listWidget_B;
        if (s.rank()==Sheep::RankC) lw = ui->listWidget_C;
        
        QListWidgetItem * item = new QListWidgetItem(makeSheepItemTitle(s, s.comment()));
        // it's not necessary to manually insert the item because we set the list as the parent
        item->setData(Qt::UserRole+1, QVariant::fromValue(s.stn()));
        item->setData(Qt::UserRole+2, QVariant::fromValue(s.comment()));
        lw->addItem(item);
    }
}

QString ResultsWizardPage::makeSheepItemTitle(Sheep s, QString comment) {
    Breeder owner = m_show.breeders().value(s.owner());
    // prepare locale for date formatting:
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);
    QString sheepinfo = tr("%1\n%2 %3 %4\n%5")
             .arg(Sheep::shortName(s.stn()))
             .arg(owner.initials())
             .arg(owner.prefixes())
             .arg(owner.name())
             .arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy"));
    if (comment.length() > 0) {
        return QString("%1\n%2").arg(sheepinfo).arg(comment);
    } else {
        return sheepinfo;
    }
}

void ResultsWizardPage::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
//    qDebug() << "double clicked: " << item->text();
    QString sheepid = item->data(Qt::UserRole+1).value<QString>();
//    qDebug() << "sheep id: " << sheepid;
    Sheep s = m_show.sheep().value(sheepid);
//    qDebug() << "sheep info: " << s.id();
    CommentDialog d;
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);
    d.setSheepName(QString("%1 - %2").arg(Sheep::shortName(s.stn())).arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy")));
    d.setComment(item->data(Qt::UserRole+2).value<QString>());
    if (d.exec()== QDialog::Accepted) {
        item->setData(Qt::UserRole+2, d.getComment());
        item->setText(makeSheepItemTitle(s, d.getComment()));
    }
}

QList<CustomException> ResultsWizardPage::validate() {
    QList<CustomException> res;
    if (ui->listWidget_none->count()>0) {
        res.append(CustomWarning(tr("Niet alle %1 zijn beoordeeld.").arg(title().toLower())));
    }
    return res;
}


void ResultsWizardPage::saveList(QListWidget * list, Sheep::Rank rank) {
    for (int i=0 ; i < list->count() ; i++) {
        QListWidgetItem* it = list->item(i);
        QString id = it->data(Qt::UserRole+1).value<QString>();
        Sheep s = m_show.sheep().value(id);
        s.setRank(rank);
        s.setPlace(i);
        s.setComment(it->data(Qt::UserRole+2).value<QString>());
    }
}

void ResultsWizardPage::save()
{
    saveList(ui->listWidget_none, Sheep::RankNone);
    saveList(ui->listWidget_A, Sheep::RankA);
    saveList(ui->listWidget_B, Sheep::RankB);
    saveList(ui->listWidget_C, Sheep::RankC);
}
