#include "wizardconfirmationpage.h"
#include "ui_wizardconfirmationpage.h"


WizardConfirmationPage::WizardConfirmationPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::WizardConfirmationPage)
{
    ui->setupUi(this);
}

WizardConfirmationPage::~WizardConfirmationPage()
{
    delete ui;
}

bool WizardConfirmationPage::isComplete() const {
    return allisok;
}


void WizardConfirmationPage::setStatus(QList<CustomException> msgs) {
    allisok = true;
    
    ui->listWidget->clear();
    
    for (CustomException msg : msgs) {
        if (msg.isCritical()) {
            allisok = false;
        }
        
        QIcon icon = style()->standardIcon(msg.iconstyle());
        QListWidgetItem * item = new QListWidgetItem(icon, msg.what());
        // the item gets destroyed when the listwidget is destroyed
        // QIcon is one of the COW/implicitly shared data types so that this copy is destroyed after this scope finishes is no problem
        ui->listWidget->addItem(item);
    }
    
    emit(completeChanged());
    
}
