#include "categorywizard.h"
#include "ui_categorywizard.h"
#include "categorywizardpage.h"

#include "database.h"

#include <QDebug>
#include <QLayout>
#include <QListWidget>
#include <QHeaderView>
#include <QMessageBox>
#include <QtAlgorithms>
#include <QtMath>
#include <QSpinBox>
#include <QLabel>



CategoryWizard::CategoryWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::CategoryWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}


CategoryWizard::CategoryWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::CategoryWizard),
    m_show(show)
{
    ui->setupUi(this);
    this->showMaximized();
    
    for (Sheep s : m_show.sheep())
    {
        if (s.doesParticipate())
        {
            if (s.dateOfBirth().year() == m_show.date().year())
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_ewelambs->addSheep(s);
                else
                    ui->wizardPage_ramlambs->addSheep(s);
            }
            else if (s.dateOfBirth().year() == m_show.date().year()-1) 
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_eweyearlings->addSheep(s);
                else
                    ui->wizardPage_rams->addSheep(s);
            }
            else 
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_ewes->addSheep(s);
                else
                    ui->wizardPage_rams->addSheep(s);
            }
        }
    }
    
    ui->wizardPage_ewelambs->setShow(m_show);
    ui->wizardPage_ewes->setShow(m_show);
    ui->wizardPage_eweyearlings->setShow(m_show);
    ui->wizardPage_ramlambs->setShow(m_show);
    ui->wizardPage_rams->setShow(m_show);
    
    ui->wizardPage_ewelambs->initDistribution();
    ui->wizardPage_ewes->initDistribution();
    ui->wizardPage_eweyearlings->initDistribution();
    ui->wizardPage_ramlambs->initDistribution();
    ui->wizardPage_rams->initDistribution();
    
    
    auto changeallmaxes = [=](int i)  {
        ui->wizardPage_ewelambs->setMaxPerCategory(i);
        ui->wizardPage_eweyearlings->setMaxPerCategory(i);
        ui->wizardPage_ewes->setMaxPerCategory(i);
        ui->wizardPage_ramlambs->setMaxPerCategory(i);
        ui->wizardPage_rams->setMaxPerCategory(i);
    };
    connect(ui->wizardPage_ewelambs,     &CategoryWizardPage::maxPerCategoryEdited, changeallmaxes);
    connect(ui->wizardPage_eweyearlings, &CategoryWizardPage::maxPerCategoryEdited, changeallmaxes);
    connect(ui->wizardPage_ewes,         &CategoryWizardPage::maxPerCategoryEdited, changeallmaxes);
    connect(ui->wizardPage_ramlambs,     &CategoryWizardPage::maxPerCategoryEdited, changeallmaxes);
    connect(ui->wizardPage_rams,         &CategoryWizardPage::maxPerCategoryEdited, changeallmaxes);
    

}








CategoryWizard::~CategoryWizard()
{
    delete ui;
}



void CategoryWizard::on_CategoryWizard_accepted()
{
    ui->wizardPage_ewelambs->saveSheep(m_show);
    ui->wizardPage_ewes->saveSheep(m_show);
    ui->wizardPage_eweyearlings->saveSheep(m_show);
    ui->wizardPage_ramlambs->saveSheep(m_show);
    ui->wizardPage_rams->saveSheep(m_show);
    
    // and save to disk
    Database::instance()->saveShows();
}


void CategoryWizard::setMaxPerCategory(int max) {
    ui->wizardPage_ewelambs->setMaxPerCategory(max);
    ui->wizardPage_ewes->setMaxPerCategory(max);
    ui->wizardPage_eweyearlings->setMaxPerCategory(max);
    ui->wizardPage_ramlambs->setMaxPerCategory(max);
    ui->wizardPage_rams->setMaxPerCategory(max);
}



void CategoryWizard::on_CategoryWizard_currentIdChanged(int id)
{
    if (page(id)==ui->wizardPage_summary)
    {
        QStringList problems;
        if (ui->wizardPage_ewelambs    ->getUnassigned() > 0) problems << tr("niet alle ooilammeren zijn ingedeeld");
        if (ui->wizardPage_ewes        ->getUnassigned() > 0) problems << tr("niet alle ooien zijn ingedeeld");
        if (ui->wizardPage_eweyearlings->getUnassigned() > 0) problems << tr("niet alle ooi jaarlingen zijn ingedeeld");
        if (ui->wizardPage_ramlambs    ->getUnassigned() > 0) problems << tr("niet alle ramlammeren zijn ingedeeld");
        if (ui->wizardPage_rams        ->getUnassigned() > 0) problems << tr("niet alle rammen zijn ingedeeld");
        
        if (problems.size()==0)
        {
            ui->label->setText(tr("Klaar om op te slaan..."));
            ui->label_2->setText("✓");
            ui->label_2->setStyleSheet("QLabel { color : green; }");
            ui->label_3->setText("");
        }
        else 
        {
            ui->label->setText(problems.join("\n"));
            ui->label_2->setText("✕");
            ui->label_2->setStyleSheet("QLabel { color : red; }");
            ui->label_3->setText(tr("Je kunt je bewerkingen wel opslaan, maar de schapen die nog niet zijn ingedeeld zullen ook niet in het boekje verschijnen als je dat nu probeert te maken..."));
        }
    }
}
