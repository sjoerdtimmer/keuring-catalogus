#ifndef CATEGORYWIZARD_H
#define CATEGORYWIZARD_H

#include "datastructures/show.h"

#include <QWizard>
#include <QTableWidget>

namespace Ui {
class CategoryWizard;
}

class CategoryWizard : public QWizard
{
    Q_OBJECT

public:
    CategoryWizard(Show show, QWidget *parent = nullptr);
    explicit CategoryWizard(QWidget *parent = nullptr);
    ~CategoryWizard();

    
    void populateLists();

public slots:
    void setMaxPerCategory(int max);



private slots:
    void on_CategoryWizard_accepted();
    void on_CategoryWizard_currentIdChanged(int id);
    
private:
    Ui::CategoryWizard *ui;
    Show m_show;
    QList<QListWidget*> lists;
};

#endif // CATEGORYWIZARD_H
