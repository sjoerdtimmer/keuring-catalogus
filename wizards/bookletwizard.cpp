#include "bookletwizard.h"
#include "ui_bookletwizard.h"

#include "WordProcessingCompiler.h"
#include "WordProcessingMerger.h"
#include <exception>
#include <iostream>
#include <stdexcept>

#include <QLineEdit>
#include <QFileDialog>
#include <QtDebug>
#include <QString>
#include <QTextEdit>
#include <QMessageBox>
#include <QTemporaryDir>
#include <QStyle>
#include <QThread>

using namespace DocxFactory;
using namespace  std;


BookletThread::BookletThread(QObject *parent, Show show, QString inputfile, QString outputfile) :
    QThread(parent),
    m_show(show),
    inputfile(inputfile),
    outputfile(outputfile)
{
        
}
    
void BookletThread::run() {
    makeBooklet();
}
    
void BookletThread::makeBooklet()
{
    try
    {
        WordProcessingMerger& l_merger = WordProcessingMerger::getInstance();
        WordProcessingCompiler& l_compiler = WordProcessingCompiler::getInstance();

        // create a temporary dir for compiling and executing the template
        QTemporaryDir tempdir;
        tempdir.setAutoRemove(false);
        if (!tempdir.isValid())
        {
            throw logic_error("failed to create a writable temp dir");
        }

//            std::string in        = ui->lineEdit_template->text().toStdString();
        std::string compiledtemplate = QString("%1/%2").arg(tempdir.path()).arg("template.dfw").toStdString();
//            std::string out       = ui->lineEdit_output->text().toStdString();

        // compile input docx to dfw template
        l_compiler.compile(inputfile.toStdString(), compiledtemplate);

        // load dfw for merging
        l_merger.load(compiledtemplate);

        // create a locale
        QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

        // insert auto table-of-contents
        l_merger.setUpdateTocMethod(2);
        l_merger.paste("toc");
        
        makeParticipantsPage(l_merger);
        
        int catcounter = 1;
        int sheepcounter = 1;
        
        makeCategory(l_merger, tr("ooilammeren"),     &catcounter, &sheepcounter, filterSheep(m_show.date().year(), m_show.date().year(), Sheep::Ewe));
        makeCategory(l_merger, tr("ramlammeren"),     &catcounter, &sheepcounter, filterSheep(m_show.date().year(), m_show.date().year(), Sheep::Ram));

        makeCategory(l_merger, tr("jaarling ooien"),  &catcounter, &sheepcounter, filterSheep(m_show.date().year()-1, m_show.date().year()-1, Sheep::Ewe));
        makeCategory(l_merger, tr("jaarling rammen"), &catcounter, &sheepcounter, filterSheep(m_show.date().year()-1, m_show.date().year()-1, Sheep::Ram));

        makeCategory(l_merger, tr("ooien"),           &catcounter, &sheepcounter, filterSheep(m_show.date().year()-99, m_show.date().year()-2, Sheep::Ewe));
        makeCategory(l_merger, tr("rammen"),          &catcounter, &sheepcounter, filterSheep(m_show.date().year()-99, m_show.date().year()-2, Sheep::Ram));

        QList<Sheep> sheep;
        for (Sheep s : m_show.sheep())
        { if (s.doesParticipate() && s.doesPairOfRam()) sheep.append(s); }
        if (sheep.count()>0) {        
            makeSpecialPage(l_merger, tr("Tweetal van één ram"), &catcounter, &sheepcounter, sheep);
        }
        
        sheep.clear();
        for (Sheep s : m_show.sheep())
        { if (s.doesParticipate() && s.doesTripleOfRam()) sheep.append(s); }
        if (sheep.count()>0) {        
            makeSpecialPage(l_merger, tr("Drietal van één ram"), &catcounter, &sheepcounter, sheep);
        }
        
        sheep.clear();        
        for (Sheep s : m_show.sheep()) {if (s.doesParticipate() && s.doesCompanyThree()) { sheep.append(s); }}
        if (sheep.count()>0) {
            makeSpecialPage(l_merger, tr("Bedrijfspresentatie"), &catcounter, &sheepcounter, sheep);
        }
        
        sheep.clear();
        for (Sheep s : m_show.sheep())
        { 
            if (s.doesParticipate() && !s.childPresentation().trimmed().isEmpty()) 
            {
                sheep.append(s);
            }
        }
        if (sheep.count()>0) {
            makeChildPresentationPage(l_merger, &catcounter, &sheepcounter, sheep);
        }
        sheep.clear();
        
        
        
        // saving output
        l_merger.save(outputfile.toStdString());
        
        emit done();
//            ui->label_error->setText("✔");
//            ui->label_error->setStyleSheet("QLabel { color : green; }");
//            ui->label_errordetail->setText("Succesvol opgeslagen!");
    }
    catch (const exception& p_exception)
    {
        emit error(p_exception.what());
//            cout << p_exception.what() << endl;
//            ui->label_error->setText("✖");
//            ui->label_error->setStyleSheet("QLabel { color : red; }");
//            ui->label_errordetail->setText(p_exception.what());
    }

}
    
    
QList<Sheep> BookletThread::filterSheep(int minyear, int maxyear, Sheep::Gender gender)
{
    QList<Sheep> sheep;
    for (Sheep s : m_show.sheep())
    {
        if (s.doesParticipate() && s.gender() == gender && s.dateOfBirth().year()>=minyear && s.dateOfBirth().year()<=maxyear)
        {
            sheep.append(s);
        }
    }
    return sheep;
}
    
void BookletThread::makeParticipantsPage(WordProcessingMerger& l_merger) 
{
    l_merger.setClipboardValue("deelnemerslijst", "keuringnaam", m_show.name().toStdString());
    l_merger.paste("deelnemerslijst");
    
    // first loop through the sheep to find which owners to include:
    QSet<QString> participatingbreederids;
    for(Sheep s : m_show.sheep())
    {
        if (s.doesParticipate()) 
        {
            participatingbreederids.insert(s.owner());
        }
    }
    
    // do the map trick to get them in sorted order
    QMultiMap<QString, Breeder> owners;
    for (QString breederid : participatingbreederids) {
        Breeder b = m_show.breeders().value(breederid);
        owners.insert(b.name(), b);
    }
    
    int i = 1;
    for (Breeder b : owners) 
    {
        QString fullOwnerName   = QString("%1 %2 %3").arg(b.initials()).arg(b.prefixes()).arg(b.name()).simplified();
        l_merger.setClipboardValue("deelnemer", "seq", QString("%1").arg(i++).toStdString());
        l_merger.setClipboardValue("deelnemer", "naam", fullOwnerName.toStdString());
        l_merger.setClipboardValue("deelnemer", "adres", b.address().toStdString());
        l_merger.setClipboardValue("deelnemer", "woonplaats", b.city().toStdString());
        l_merger.setClipboardValue("deelnemer", "telefoon", b.phone().toStdString());
        l_merger.paste("deelnemer");
    }
}
    
void BookletThread::makeCategory(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep)
{
    // max category number equals number of required pages
    int numpages = 0;
    for (Sheep s : sheep)
    {
        if (s.subcategory() > numpages) numpages = s.subcategory();
    }

    for (int cat=1; cat<=numpages; cat++)
    {
        QList<Sheep> subcatsheep;
        for (Sheep s : sheep)
        {
            if (s.subcategory()==cat) subcatsheep.append(s);
        }
        makeCategoryPage(l_merger, name, catcounter, sheepcounter, subcatsheep);
    }
}
    
void BookletThread::makeCategoryPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep)
{
    // create a locale
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

    // the easy replacements
    l_merger.setClipboardValue("rubriek", "catcount", QString("%1").arg((*catcounter)++).toStdString());
    l_merger.setClipboardValue("rubriek", "catname", name.toStdString());
    l_merger.paste("rubriek");

    // recreate sort order:
    std::sort(sheep.begin(), sheep.end(), [=](const Sheep s1, const Sheep s2)
    {
        return s1.order()< s2.order();
    });

    // insert sheep
    for (Sheep s : sheep)
    {
        Breeder owner   = m_show.breeders().value(s.owner());
        Breeder breeder = m_show.breeders().value(s.breeder());
        QString fullOwnerName   = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
        QString fullBreederName = QString("%1 %2 %3").arg(breeder.initials()).arg(breeder.prefixes()).arg(breeder.name()).simplified();
        if (! m_show.breeders().contains(s.breeder())) {
            qDebug() << "breeder " << s.breeder() << " of " << s.stn() << "not known!";
            fullBreederName = tr("niet bekend");
        }
        
        l_merger.setClipboardValue("schaap", "seq", QString("%1").arg((*sheepcounter)++).toStdString());
        l_merger.setClipboardValue("schaap", "dier_stn", Sheep::shortName(s.stn()).toStdString());
        l_merger.setClipboardValue("schaap", "dier_uln", s.uln().toStdString());
        l_merger.setClipboardValue("schaap", "vader_stn", Sheep::shortName(s.father()).toStdString());
        l_merger.setClipboardValue("schaap", "moeder_stn", Sheep::shortName(s.mother()).toStdString());
        l_merger.setClipboardValue("schaap", "dier_geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
        l_merger.paste("schaap");
        
        if (s.owner()==s.breeder()) {
            l_merger.setClipboardValue("breederowner", "naam", fullOwnerName.toStdString());
            l_merger.setClipboardValue("breederowner", "plaats", owner.city().toStdString());    
            l_merger.paste("breederowner");
        } else {
            if (s.breeder().trimmed().length() > 0) {
                l_merger.setClipboardValue("breeder", "naam", fullBreederName.toStdString());
                l_merger.setClipboardValue("breeder", "plaats", breeder.city().toStdString());    
            }
            else {
                l_merger.setClipboardValue("breeder", "naam", "niet bekend");
                l_merger.setClipboardValue("breeder", "plaats", "");
            }
            l_merger.paste("breeder");
            // assumption: owner is always known
            l_merger.setClipboardValue("owner", "naam", fullOwnerName.toStdString());
            l_merger.setClipboardValue("owner", "plaats", owner.city().toStdString());    
            l_merger.paste("owner");
        } 
        
        
    }
}
    
void BookletThread::makeSpecialPage(WordProcessingMerger& l_merger, QString name, int * catcounter, int * sheepcounter, QList<Sheep> sheep)
{
    // create a locale
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

    // create page with header:
    l_merger.setClipboardValue("rubriek", "catcount", QString("%1").arg((*catcounter)++).toStdString());
    l_merger.setClipboardValue("rubriek", "catname", name.toStdString());
    l_merger.paste("rubriek");

    // sort such that farmers are grouped and sheep are ordered by date of birth
    std::sort(sheep.begin(), sheep.end(), [=](const Sheep s1, const Sheep s2)
    {
        return s1.dateOfBirth() < s2.dateOfBirth();
    });
    
    std::sort(sheep.begin(), sheep.end(), [=](const Sheep s1, const Sheep s2)
    {
        return s1.owner().compare(s2.owner()) < 0;
//        return s1->dateOfBirth < s2->dateOfBirth;
    });
    
    QString prevowner = QString("");
    for(Sheep s : sheep)
    {
        if (prevowner != s.owner()) {
            // create group
            Breeder owner   = m_show.breeders().value(s.owner());
            Breeder breeder = m_show.breeders().value(s.breeder());
            QString fullOwnerName   = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
            QString fullBreederName = QString("%1 %2 %3").arg(breeder.initials()).arg(breeder.prefixes()).arg(breeder.name()).simplified();
            l_merger.setClipboardValue("group", "naamfokker", fullBreederName.toStdString());
            l_merger.setClipboardValue("group", "naameigenaar", fullOwnerName.toStdString());
            l_merger.setClipboardValue("group", "plaatsfokker", breeder.city().toStdString());
            l_merger.setClipboardValue("group", "plaatseigenaar", owner.city().toStdString());
            l_merger.setClipboardValue("group","seq", QString("%1").arg((*sheepcounter)++).toStdString());
            l_merger.paste("group");
        }
        l_merger.setClipboardValue("sheepingroup", "dier_stn", Sheep::shortName(s.stn()).toStdString());
        l_merger.setClipboardValue("sheepingroup", "geslacht", s.gender()==Sheep::Gender::Ewe?"ooi":"ram");
        l_merger.setClipboardValue("sheepingroup", "dier_geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
        l_merger.paste("sheepingroup");
        prevowner = s.owner();
    }
}
    
void BookletThread::makeChildPresentationPage(WordProcessingMerger& l_merger, int * catcounter, int * sheepcounter, QList<Sheep> sheep)
{
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

    // create page with header:
    l_merger.setClipboardValue("rubriek", "catcount", QString("%1").arg((*catcounter)++).toStdString());
    l_merger.setClipboardValue("rubriek", "catname", "Presentaties door kinderen");
    l_merger.paste("rubriek");
    
    for(Sheep s : sheep)
    {
        Breeder owner   = m_show.breeders().value(s.owner());
        QString fullOwnerName   = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
        for (QString child : s.childPresentation().split('|'))
        {
            l_merger.setClipboardValue("kindpresentatie", "naamkind", child.toStdString());
            l_merger.setClipboardValue("kindpresentatie", "dier_stn", Sheep::shortName(s.stn()).toStdString());
            l_merger.setClipboardValue("kindpresentatie", "naameigenaar", fullOwnerName.toStdString());
            l_merger.setClipboardValue("kindpresentatie", "geslacht", s.gender()==Sheep::Ewe?"ooi":"ram");
            l_merger.setClipboardValue("kindpresentatie", "plaatseigenaar", owner.city().toStdString());
            l_merger.setClipboardValue("kindpresentatie", "dier_geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
            l_merger.paste("kindpresentatie");
        }
    }
}



BookletWizard::BookletWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::BookletWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}

BookletWizard::BookletWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::BookletWizard),
    m_show(show)
{
    ui->setupUi(this);
    this->showMaximized();
    this->setButtonText(BookletWizard::FinishButton, tr("Opslaan"));
    this->setButtonText(BookletWizard::CancelButton, tr("Sluiten"));

    // this will disconnect ALL signals from the finish button
    this->disconnect(button(BookletWizard::FinishButton), nullptr, this, nullptr);
    this->connect(button(BookletWizard::FinishButton), &QPushButton::clicked, [=]()
    {
       cout << "finish button clicked" << endl;
       ui->wizardPage1->setEnabled(false);
       button(QWizard::FinishButton)->setEnabled(false);
       button(QWizard::CancelButton)->setEnabled(false);
       BookletThread * worker = new BookletThread(this, m_show, ui->lineEdit_template->text(), ui->lineEdit_output->text()); 
       worker->start();
       connect(worker, &QThread::started, [](){
           qDebug() << "worker started";
       });
       connect(worker, &BookletThread::done, [=]() {
           qDebug() << "worker finished";
           ui->label_error->setText("✔");
           ui->label_error->setStyleSheet("QLabel { color : green; }");
           ui->label_errordetail->setText("Succesvol opgeslagen!");
           button(QWizard::FinishButton)->setEnabled(true);
           button(QWizard::CancelButton)->setEnabled(true);
           ui->wizardPage1->setEnabled(true);
       });
       connect(worker, &BookletThread::error, [=](QString msg){
           qDebug() << "worker ran into error: " << msg;
           ui->label_error->setText("✖");
           ui->label_error->setStyleSheet("QLabel { color : red; }");
           ui->label_errordetail->setText(msg);
           button(QWizard::FinishButton)->setEnabled(true);
           button(QWizard::CancelButton)->setEnabled(true);
           ui->wizardPage1->setEnabled(true);
       });
      
    });
   
}


BookletWizard::~BookletWizard()
{
    delete ui;
}



void BookletWizard::on_pushButton_browsetemplate_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Template openen voor boekje..."), tr("boekje-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_template->setText(fileName);
}

void BookletWizard::on_pushButton_browseoutput_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Output bestand voor boekje..."), tr("boekje.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_output->setText(fileName);
}

void BookletWizard::on_pushButton_exporttemplate_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Template voor boekje opslaan..."), tr("boekje-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    if (!fileName.endsWith(".docx", Qt::CaseInsensitive))
    {
        fileName.append(".docx");
    }
    if (!QFile::copy(":/templates/booklet-template.docx", fileName))
    {
        qWarning() << tr("failed to write template to file '%1'").arg(fileName);
    }
    else 
    {
        ui->lineEdit_template->setText(fileName);
    }
}




