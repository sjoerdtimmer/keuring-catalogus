#ifndef INVITATIONSWIZARD_H
#define INVITATIONSWIZARD_H

#include <QWizard>
#include "datastructures/show.h"

namespace Ui {
class InvitationsWizard;
}

class InvitationsWizard : public QWizard
{
    Q_OBJECT

public:
    explicit InvitationsWizard(QWidget *parent = nullptr);
    InvitationsWizard(Show show, QWidget *parent = nullptr);
    void makeInvitations();

    ~InvitationsWizard();

private slots:
    void on_pushButton_browseTemplate_clicked();

    void on_pushButton_exporttemplate_clicked();

    void on_pushButton_browseOutput_clicked();

private:
    Ui::InvitationsWizard *ui;
    Show m_show;
};

#endif // INVITATIONSWIZARD_H
