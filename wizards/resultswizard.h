#ifndef RESULTSWIZARD_H
#define RESULTSWIZARD_H

#include "datastructures/show.h"

#include <QWizard>
#include <QTableWidget>

namespace Ui {
class ResultsWizard;
}

class ResultsWizard : public QWizard
{
    Q_OBJECT
    
public:
    ResultsWizard(Show show, QWidget *parent = nullptr);
    explicit ResultsWizard(QWidget *parent = nullptr);
    ~ResultsWizard();
    
private slots:
    void on_ResultsWizard_currentIdChanged(int id);
    
    void on_ResultsWizard_accepted();
    
private:
    void populateLists();
    
    Ui::ResultsWizard *ui;
    Show m_show;
    
};

#endif // RESULTSWIZARD_H
