#include "invitationswizard.h"
#include "ui_invitationswizard.h"

#include "WordProcessingCompiler.h"
#include "WordProcessingMerger.h"
#include <exception>
#include <iostream>
#include <ctime>

#include <QLineEdit>
#include <QFileDialog>
#include <QtDebug>
#include <QString>
#include <QStringBuilder>
#include <QTextEdit>
#include <QMessageBox>
#include <QTemporaryDir>
#include <QStyle>

#include <iostream>
#include <stdexcept>
using namespace DocxFactory;
using namespace  std;
//using std::cout;
//using std::endl;

InvitationsWizard::InvitationsWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::InvitationsWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}

InvitationsWizard::InvitationsWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::InvitationsWizard),
    m_show(show)
{
    ui->setupUi(this);
    this->showMaximized();
    this->setButtonText(InvitationsWizard::FinishButton, tr("Opslaan"));
    this->setButtonText(InvitationsWizard::CancelButton, tr("Sluiten"));

    // this will disconnect ALL signals from the finish button
    this->disconnect(button(InvitationsWizard::FinishButton), nullptr, this, nullptr);
    this->connect(button(InvitationsWizard::FinishButton), &QPushButton::clicked, [=]()
    {
       cout << "finish button clicked" << endl;
       makeInvitations();
    });

//    QIcon icon = QApplication::style()->standardIcon(QStyle::SP_DialogApplyButton);
//    ui->label_3->setPixmap(icon.pixmap(64,64));
//    ui->label_2->setText("✔");

}

InvitationsWizard::~InvitationsWizard()
{
    delete ui;
}

void InvitationsWizard::on_pushButton_browseTemplate_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,tr("Template openen voor brief..."), tr("brieven-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_template->setText(fileName);
}

void InvitationsWizard::on_pushButton_exporttemplate_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Template voor brief opslaan..."), tr("brieven-template.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    if (!fileName.endsWith(".docx", Qt::CaseInsensitive))
    {
        fileName.append(".docx");
    }
    if (!QFile::copy(":/templates/brieven-template.docx", fileName))
    {
        qWarning() << tr("failed to write template to file '%1'").arg(fileName);
    }
    else 
    {
        ui->lineEdit_template->setText(fileName);
    }
}

void InvitationsWizard::on_pushButton_browseOutput_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,tr("Output bestand voor brieven..."), tr("brieven.docx"), tr("Word document (*.docx)"));
    if (fileName.isEmpty())
    {
        return;
    }
    ui->lineEdit_output->setText(fileName);
}


void InvitationsWizard::makeInvitations()
{
    try
    {
        WordProcessingMerger& l_merger = WordProcessingMerger::getInstance();
        WordProcessingCompiler& l_compiler = WordProcessingCompiler::getInstance();

        // create a temporary dir for compiling and executing the template
        QTemporaryDir tempdir;
        tempdir.setAutoRemove(false);
        if (!tempdir.isValid())
        {
            throw logic_error("failed to create a writable temp dir");
        }

        std::string inputfile        = ui->lineEdit_template->text().toStdString();
        std::string compiledtemplate = QString("%1/%2").arg(tempdir.path()).arg("template.dfw").toStdString();
//        std::string outputfile       = QString("%1/%2").arg(tempdir.path()).arg("output.docx").toStdString();
        std::string outputfile       = ui->lineEdit_output->text().toStdString();

        // compile input docx to dfw template
        l_compiler.compile(inputfile, compiledtemplate);

        // load dfw for merging
        l_merger.load(compiledtemplate);

        // create a locale
        QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);

        // build a disctionary of sheep:
        // also check which special categories there are
        bool showHasTwoFromOneRam = false;
        bool showHasThreeFromOneRam = false;
        bool showHasCompanyThree = false;
        bool showHasChildPresentation = false;

        QMultiHash<QString, Sheep> sheepbyowner;
        for (Sheep s : m_show.sheep())
        {
            if (s.doesParticipate())
            {
                sheepbyowner.insert(s.owner(), s);
                if (s.doesCompanyThree()) showHasCompanyThree = true;
                if (s.doesPairOfRam()) showHasTwoFromOneRam = true;
                if (s.doesTripleOfRam()) showHasThreeFromOneRam = true;
                if (!s.childPresentation().trimmed().isEmpty()) showHasChildPresentation = true;
            }
        }

        // make a set of unique owner names (sheepbyowner.keys() contains duplicates)
        QSet<QString> owners;
        for(QString owner : sheepbyowner.keys())
        {
            owners.insert(owner);
        }

        // for each owner print one page
        for (QString ownerId : owners)
        {
            qDebug() << "writing letter for " << ownerId << endl;
//            Sheep s = sheepbyowner.value(ownerId);
            Breeder owner = m_show.breeders().value(ownerId);
            QString fullName = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
            
            l_merger.setClipboardValue("brief", "naamfokker",     fullName.toStdString());
            l_merger.setClipboardValue("brief", "adresfokker",    owner.address().toStdString());
            l_merger.setClipboardValue("brief", "postcodefokker", owner.postalcode().toStdString());
            l_merger.setClipboardValue("brief", "plaatsfokker",   owner.city().toStdString());
            l_merger.setClipboardValue("brief", "datum",          locale.toString(QDate::currentDate(), "d MMMM yyyy").toStdString());
            l_merger.setClipboardValue("brief", "naamkeuring",    m_show.name().toStdString());
            l_merger.setClipboardValue("brief", "datumkeuring",   locale.toString(m_show.date(), "d MMMM yyyy").toStdString());
            l_merger.paste("brief");

            // the whole list of sheep:
            // also determine which categories this breeder is participating in
            bool doesBedrijfsdrietal = false;
            bool doesDrieVanEenRam = false;
            bool doesTweeVanEenRam = false;
            bool doesPresentatieDoorKind = false;
            
            
            // sort sheep by date of birth
            QMultiMap<QDate, Sheep> sortmap;
            for (Sheep s : sheepbyowner.values(ownerId)) {
                sortmap.insert(s.dateOfBirth(), s);
            }
            QList<Sheep> m_sheep = sortmap.values();
            
            for (Sheep s : m_sheep)
            {
                if (s.doesParticipate())
                {
                    l_merger.setClipboardValue("doetwelmee", "onderdeel", "deze keuring");
                    l_merger.setClipboardValue("schaap", "schaapnummer",  Sheep::shortName(s.stn()).toStdString());
                    l_merger.setClipboardValue("schaap", "geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
                    l_merger.setClipboardValue("schaap", "cat", s.subcategory());
                    l_merger.paste("schaap");
                    if (s.doesCompanyThree()) doesBedrijfsdrietal = true;
                    if (s.doesTripleOfRam()) doesDrieVanEenRam = true;
                    if (s.doesPairOfRam()) doesTweeVanEenRam = true;
                    if (!s.childPresentation().trimmed().isEmpty()) doesPresentatieDoorKind = true;
                }
            }

            // now for each separate category
            if (showHasCompanyThree)
            {
                if (doesBedrijfsdrietal)
                {
                    l_merger.setClipboardValue("doetwelmee", "onderdeel", "het beste bedrijfsdrietal");
                    l_merger.paste("doetwelmee");
                    for (Sheep s : m_sheep)
                    {
                        if (s.doesParticipate() && s.doesCompanyThree())
                        {
                            l_merger.setClipboardValue("schaap", "schaapnummer", Sheep::shortName(s.stn()).toStdString());
                            l_merger.setClipboardValue("schaap", "geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
                            l_merger.paste("schaap");
                        }
                    }
                }
                else
                {
                    l_merger.setClipboardValue("doetnietmee", "onderdeel", "het beste bedrijfsdrietal");
                    l_merger.paste("doetnietmee");
                }
            }

            // drie van 1 ram
            if (showHasThreeFromOneRam)
            {
                if (doesDrieVanEenRam)
                {
                    l_merger.setClipboardValue("doetwelmee", "onderdeel", "het beste drietal van 1 ram");
                    l_merger.paste("doetwelmee");
                    for (Sheep s : m_sheep)
                    {
                        if (s.doesParticipate() && s.doesTripleOfRam())
                        {
                            l_merger.setClipboardValue("schaap", "schaapnummer",  Sheep::shortName(s.stn()).toStdString());
                            l_merger.setClipboardValue("schaap", "geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
                            l_merger.paste("schaap");
                        }
                    }
                }
                else
                {
                    l_merger.setClipboardValue("doetnietmee", "onderdeel", "het beste drietal van 1 ram");
                    l_merger.paste("doetnietmee");
                }
            }


            // twee van 1 ram
            if (showHasTwoFromOneRam)
            {
                if (doesTweeVanEenRam)
                {
                    l_merger.setClipboardValue("doetwelmee", "onderdeel", "het beste tweetal van 1 ram");
                    l_merger.paste("doetwelmee");
                    for (Sheep s : m_sheep)
                    {
                        if (s.doesParticipate() && s.doesPairOfRam())
                        {
                            l_merger.setClipboardValue("schaap", "schaapnummer",  Sheep::shortName(s.stn()).toStdString());
                            l_merger.setClipboardValue("schaap", "geboortedatum", locale.toString(s.dateOfBirth(), "d MMMM yyyy").toStdString());
                            l_merger.paste("schaap");
                        }
                    }
                }
                else
                {
                    l_merger.setClipboardValue("doetnietmee", "onderdeel", "het beste tweetal van 1 ram");
                    l_merger.paste("doetnietmee");
                }
            }


            // presentatie door kind:
            if (showHasChildPresentation)
            {
                if (doesPresentatieDoorKind)
                {
                    for (Sheep s : m_sheep)
                    {
                        if (s.doesParticipate() && ! s.childPresentation().trimmed().isEmpty())
                        {
                            for (QString child : s.childPresentation().split('|'))
                            {
                                l_merger.setClipboardValue("doetwelmeemetkind", "schaapnummer",  Sheep::shortName(s.stn()).toStdString());
                                l_merger.setClipboardValue("doetwelmeemetkind", "naamkind", child.toStdString());
                                l_merger.paste("doetwelmeemetkind");    
                            }
                        }
                    }
                }
                else
                {
                    l_merger.setClipboardValue("doetnietmee", "onderdeel", "een presentatie door een kind");
                    l_merger.paste("doetnietmee");
                }
            }
        }

        // saving output
        l_merger.save(outputfile);

        ui->label_error->setText("✔");
        ui->label_error->setStyleSheet("QLabel { color : green; }");
        ui->label_errordetail->setText("Succesvol opgeslagen!");

    }
    catch (const exception& p_exception)
    {
        cout << p_exception.what() << endl;
        ui->label_error->setText("✖");
        ui->label_error->setStyleSheet("QLabel { color : red; }");
        ui->label_errordetail->setText(p_exception.what());
    }
}

