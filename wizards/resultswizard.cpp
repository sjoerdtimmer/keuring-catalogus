#include "resultswizard.h"
#include "ui_resultswizard.h"

#include <database.h>

ResultsWizard::ResultsWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::ResultsWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}

ResultsWizard::~ResultsWizard()
{
    delete ui;
}

ResultsWizard::ResultsWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::ResultsWizard),
    m_show(show)
{
    ui->setupUi(this);
    this->showMaximized();
    
    for (Sheep s : m_show.sheep())
    {
        if (s.doesParticipate())
        {
            if (s.dateOfBirth().year() == m_show.date().year())
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_ewelambs->addSheep(s);
                else
                    ui->wizardPage_ramlambs->addSheep(s);
            }
            else if (s.dateOfBirth().year() == m_show.date().year()-1) 
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_eweyearlings->addSheep(s);
                else
                    ui->wizardPage_rams->addSheep(s);
            }
            else 
            {
                if (s.gender() == Sheep::Ewe)
                    ui->wizardPage_ewes->addSheep(s);
                else
                    ui->wizardPage_rams->addSheep(s);
            }
        }
    }

    // Note: order is important: setShow will take the sheep added above and insert them into gui
    ui->wizardPage_ewelambs->setShow(m_show);
    ui->wizardPage_ewes->setShow(m_show);
    ui->wizardPage_eweyearlings->setShow(m_show);
    ui->wizardPage_ramlambs->setShow(m_show);
    ui->wizardPage_rams->setShow(m_show);
    
    
}

void ResultsWizard::on_ResultsWizard_currentIdChanged(int id)
{
    if (page(id)==ui->wizardPage_summary) {
        QList<CustomException> problems;
        problems.append(ui->wizardPage_ewelambs->validate());
        problems.append(ui->wizardPage_ramlambs->validate());
        problems.append(ui->wizardPage_eweyearlings->validate());
        problems.append(ui->wizardPage_ewes->validate());
        problems.append(ui->wizardPage_rams->validate());
        
        if (problems.size()==0)
        {
            problems.append(CustomAcceptance(tr("Klaar om op te slaan...")));
        }
        ui->wizardPage_summary->setStatus(problems);
    }
}

void ResultsWizard::on_ResultsWizard_accepted()
{
    ui->wizardPage_ewes->save();
    ui->wizardPage_rams->save();
    ui->wizardPage_ewelambs->save();
    ui->wizardPage_ramlambs->save();
    ui->wizardPage_eweyearlings->save();
    
    // and save to disk
    Database::instance()->saveShows();
}
