#ifndef REGISTRATIONWIZARD_H
#define REGISTRATIONWIZARD_H

#include "datastructures/show.h"
#include <QWizard>
#include <QComboBox>
#include <QTabWidget>
#include <QTableWidgetItem>

namespace Ui {
class RegistrationWizard;
}

class RegistrationWizard : public QWizard
{
    Q_OBJECT

public:
    explicit RegistrationWizard(QWidget *parent = nullptr);
    RegistrationWizard(Show show, QWidget *parent = nullptr);
    ~RegistrationWizard();
    void updateSheepComboBoxes();
    void applyPreviouslySaved();
    void ensurePresenceInComboBox(Sheep s, QComboBox* combobox, bool present);
    
private slots:
    
    void on_RegistrationWizard_currentIdChanged(int id);

    void on_table_sheepparticipating_itemSelectionChanged();

    void on_RegistrationWizard_accepted();

    void on_comboBox_breeder_activated(int index);
    
    void on_tableWidget_children_itemChanged(QTableWidgetItem *item);
    
    void on_tableWidget_children_cellChanged(int row, int column);
    
    void on_tableWidget_children_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn);
    
private:
    Ui::RegistrationWizard *ui;
    Show m_show;
    bool hasgonebeyondpageone;
    //    QSet<Sheep> m_dirty_sheep;
    void makeChildRow();
};

#endif // REGISTRATIONWIZARD_H
