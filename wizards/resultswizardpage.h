#ifndef RESULTSWIZARDPAGE_H
#define RESULTSWIZARDPAGE_H

#include "datastructures/show.h"
#include <QWizardPage>
#include <exceptions.h>

namespace Ui {
class ResultsWizardPage;
}

class ResultsWizardPage : public QWizardPage
{
    Q_OBJECT
    
public:
    explicit ResultsWizardPage(QWidget *parent = nullptr);
    ~ResultsWizardPage();
    
    void setShow(Show s);
    void addSheep(Sheep s);
    QList<CustomException> validate();
    void save();
    
private slots:
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);
    
private:
    Ui::ResultsWizardPage *ui;
    QList<Sheep> m_sheep;
    Show m_show;
    void saveList(QListWidget *list, Sheep::Rank rank);
    QString makeSheepItemTitle(Sheep s, QString comment = QString(""));
};

#endif // RESULTSWIZARDPAGE_H
