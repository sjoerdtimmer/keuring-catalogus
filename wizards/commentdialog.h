#ifndef COMMENTDIALOG_H
#define COMMENTDIALOG_H

#include <QDialog>

namespace Ui {
class CommentDialog;
}

class CommentDialog : public QDialog
{
    Q_OBJECT
    
public:
    explicit CommentDialog(QWidget *parent = nullptr);
    ~CommentDialog();
    
    void setComment(QString txt);
    QString getComment() const;
    
    void setSheepName(QString name);
private:
    Ui::CommentDialog *ui;
};

#endif // COMMENTDIALOG_H
