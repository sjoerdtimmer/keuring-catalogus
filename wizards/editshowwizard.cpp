#include "wizards/editshowwizard.h"
#include "ui_editshowwizard.h"
#include "database.h"
#include "exceptions.h"

#include <QComboBox>
#include <QFileDialog>
#include <QStandardPaths>
#include <QVariant>
#include <QRadioButton>
#include <QStyle>
#include <QDebug>
#include <QFile>


EditShowWizard::EditShowWizard(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::EditShowWizard)
{
    ui->setupUi(this);
    this->showMaximized();
}

EditShowWizard::EditShowWizard(Show s, QWidget *parent) :
    QWizard(parent),
    m_show(s),
    ui(new Ui::EditShowWizard)
{
    ui->setupUi(this);
    this->showMaximized();

    // set fields:
    ui->lineEdit_showName->setText(m_show.name());
    ui->dateEdit_showDate->setDate(m_show.date());
    
    
    
    // populate list of shows:
    for (Show s : Database::instance()->shows)
    {
        if (s.name() == m_show.name()) continue;
        ui->comboBox_copyFrom->addItem(s.name(), QVariant::fromValue(s));
    }
    ui->comboBox_copyFrom->setCurrentIndex(-1);

}



EditShowWizard::~EditShowWizard()
{
    delete ui;
}

void EditShowWizard::on_pushButton_browseSheepFile_clicked()
{
    QStringList locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    QString location = "";
    if (locations.size()>0) location = locations[0]; 
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open CSV database"), location, tr("Comma or tab separated value files (*.csv *.tsv)"));
    ui->lineEdit_sheepFileName->setText(fileName);
    ui->radioButton_readSheep->setChecked(true);
}

void EditShowWizard::on_pushButton_browseBreedersFile_clicked()
{
    QStringList locations = QStandardPaths::standardLocations(QStandardPaths::HomeLocation);
    QString location = "";
    if (locations.size()>0) location = locations[0]; 
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open CSV database"), location, tr("Comma or tab separated value files (*.csv *.tsv)"));
    ui->lineEdit_breedersFileName->setText(fileName);
    ui->radioButton_readBreeders->setChecked(true);
}

void EditShowWizard::on_comboBox_copyFrom_activated(const QString &arg1)
{
    ui->radioButton_copySheep->setChecked(true);
}

void EditShowWizard::on_EditShowWizard_accepted()
{
    m_show.setName(ui->lineEdit_showName->text());
    m_show.setDate(ui->dateEdit_showDate->date());
    m_show.setBreeders(newbreeders);
    m_show.setSheep(newsheep);
//    qDebug() << "first sheep in new show: " << m_show.sheep().begin().value().shortName();
    // sync to files
    Database::instance()->saveShows();
}

void EditShowWizard::loadBreedersFromFile(QString filename, QList<CustomException>& warnings)
{
    QFile breedersfile(filename);
    if(!breedersfile.exists())
        throw CustomException(tr("Het opgegeven fokkers bestand bestaat niet. "));
    else if (!breedersfile.open(QIODevice::ReadOnly))
        throw CustomException(tr("Het opgegeven fokkers bestand kan niet geopend worden. "));

    newbreeders.clear();
    // read breeders from file:
    QTextStream in(&breedersfile);

    QString delimiter;
    // get headers:
    QString headerline = in.readLine();
    QStringList headers;
    int numkomma     = headerline.split(",").size();
    int numsemicolon = headerline.split(";").size();
    int numtab       = headerline.split("\t").size();
    if (numtab > numkomma && numtab > numsemicolon)
    {
        // file is tab separated
        delimiter = "\t";
    }
    
    else if (numkomma > numtab && numkomma > numsemicolon)
    {
        delimiter = ",";
    }
    else
    {
        delimiter = ";";
    }
    
    if (filename.endsWith("tsv", Qt::CaseInsensitive) && delimiter==",")
        warnings.append(CustomWarning(tr("Het bestand heeft de TSV extensie maar lijkt toch een CSV bestand te zijn.")));
    if (filename.endsWith("csv", Qt::CaseInsensitive) && delimiter=="\t")
        warnings.append(CustomWarning(tr("Het bestand heeft de CSV extensie maar lijkt toch een TSV bestand te zijn.")));
    
    headers = headerline.split(delimiter);
    
    
    if ( !headers.contains("ubn") )            throw CustomException(tr("header 'ubn' niet gevonden"));
    if ( !headers.contains("deb.nr") )         throw CustomException(tr("header 'deb.nr' niet gevonden"));
    if ( !headers.contains("titel") )          throw CustomException(tr("header 'titel' niet gevonden"));
    if ( !headers.contains("letters") )        throw CustomException(tr("header 'letters' niet gevonden"));
    if ( !headers.contains("voorv") )          throw CustomException(tr("header 'voorv' niet gevonden"));
    if ( !headers.contains("naam") )           throw CustomException(tr("header 'naam' niet gevonden"));
    if ( !headers.contains("adres") )          throw CustomException(tr("header 'adres' niet gevonden"));
    if ( !headers.contains("postcode") )       throw CustomException(tr("header 'postcode' niet gevonden"));
    if ( !headers.contains("plaats") )         throw CustomException(tr("header 'plaats' niet gevonden"));
    if ( !headers.contains("telefoon") )       throw CustomException(tr("header 'telefoon' niet gevonden"));
    if ( !headers.contains("e-mailadres") )    throw CustomException(tr("header 'e-mailadres' niet gevonden"));
    if ( !headers.contains("inspectieregio") ) throw CustomException(tr("header 'inspectieregio' niet gevonden"));
    
    // read the remainder:
    int linenum = 0;
    while (!in.atEnd())
    {
        linenum++;
        // read the comma or tab separated values:
        QString line = in.readLine();
        QStringList values = line.split(delimiter);

        if (values.size() != headers.size())
        {
            warnings.append(CustomNotice(tr("Regel %1 van het leden bestand bevat niet voldoende velden. Regel overgeslagen: \"%2\"").arg(linenum).arg(line)));
            continue;
        }

        Breeder b;
        QString ubn = values[headers.lastIndexOf("ubn")];
        b.setId(ubn);
        
        b.setTitle(values[headers.lastIndexOf("titel")]);
        b.setInitials(values[headers.lastIndexOf("letters")]);
        b.setPrefixes(values[headers.lastIndexOf("voorv")]);
        b.setName(values[headers.lastIndexOf("naam")]);
        b.setAddress(values[headers.lastIndexOf("adres")]);
        b.setpostalcode(values[headers.lastIndexOf("postcode")]);
        b.setCity(values[headers.lastIndexOf("plaats")]);
        b.setAbbrv(values[headers.lastIndexOf("deb.nr")]);
        b.setPhone(values[headers.lastIndexOf("telefoon")]);
        b.setEmail(values[headers.lastIndexOf("e-mailadres")]);
        b.setRegion(values[headers.lastIndexOf("inspectieregio")]);
        
        qDebug() << tr("Breeder imported: (%1 %2 %3 %4) abbrv: %5 id: %6").arg(b.title()).arg(b.initials()).arg(b.prefixes()).arg(b.name()).arg(b.abbrv()).arg(b.id());
        
        if (newbreeders.contains(ubn)) 
            warnings.append(CustomWarning(tr("Fokker met ubn %1 meerdere keren gedefinieerd").arg(ubn))); 
        else
            newbreeders.insert(b.id(), b);
    }
}

QString EditShowWizard::normalizeDateString(QString in)
{
    in.replace('/', '-');
    in = "-"+in+"-"; // will be removed later
    in.replace(QRegularExpression("-(\\d)-"), "-0\\1-");
    return in.mid(1, in.length()-2); // removes extra - at start and end
}


void EditShowWizard::loadSheepFromFile(QString filename, QList<CustomException>& exceptions)
{
    QFile sheepfile(filename);
    if(!sheepfile.exists())
        throw CustomException(tr("Het opgegeven schapen bestand bestaat niet. "));
    else if (!sheepfile.open(QIODevice::ReadOnly))
        throw CustomException(tr("Het opgegeven schapen bestand kan niet geopend worden. "));
    
    
    newsheep.clear();
    // read sheep from file:
    QTextStream in(&sheepfile);

    QString delimiter;
    // get headers:
    QString headerline = in.readLine();
    QStringList headers;
    int numkomma     = headerline.split(",").size();
    int numsemicolon = headerline.split(";").size();
    int numtab       = headerline.split("\t").size();
    if (numtab > numkomma && numtab > numsemicolon)
    {
        // file is tab separated
        delimiter = "\t";
    }
    else if (numkomma > numtab && numkomma > numsemicolon)
    {
        delimiter = ",";
    }
    else
    {
        delimiter = ";";
    }
    
    if (filename.endsWith("tsv", Qt::CaseInsensitive) && delimiter==",")
        exceptions.append(CustomWarning(tr("Het bestand heeft de TSV extensie maar lijkt toch een CSV bestand te zijn.")));
    if (filename.endsWith("csv", Qt::CaseInsensitive) && delimiter=="\t")
        exceptions.append(CustomWarning(tr("Het bestand heeft de CSV extensie maar lijkt toch een TSV bestand te zijn.")));
    
    headers = headerline.split(delimiter);
    

    if ( !headers.contains("dier_uln") )           throw CustomException(tr("header 'dier_uln' niet gevonden"));
    if ( !headers.contains("ubn_fokker") )         throw CustomException(tr("header 'ubn_fokker' niet gevonden"));
    if ( !headers.contains("ubn_houder") )         throw CustomException(tr("header 'ubn_houder' niet gevonden"));
    if ( !headers.contains("vader_stn") )          throw CustomException(tr("header 'vader_stn' niet gevonden"));
    if ( !headers.contains("moeder_stn") )         throw CustomException(tr("header 'moeder_stn' niet gevonden"));
    if ( !headers.contains("geslacht") )           throw CustomException(tr("header 'geslacht' niet gevonden"));
    if ( !headers.contains("dier_geboortedatum") ) throw CustomException(tr("header 'dier_geboortedatum' niet gevonden"));
    if ( !headers.contains("dier_stn") )           throw CustomException(tr("header 'dier_stn' niet gevonden"));
    if ( !headers.contains("staartlen") )          throw CustomException(tr("header 'staartlen' niet gevonden"));
    if ( !headers.contains("staartlen_acc") )      throw CustomException(tr("header 'staartlen_acc' niet gevonden"));

    
    //QString dateformat = this->guessDateFormat(in, delimiter, headers.lastIndexOf("dier_geboortedatum"), exceptions);
    
    // read the remainder:
    int linenum=0;
    
    QHash<QString, int> skipped_sheep;
        
    while (!in.atEnd())
    {
        linenum++;
        // read the comma or tab separated values:
        QString line = in.readLine();
        QStringList values = line.split(delimiter);

        if (values.size() != headers.size())
        {
            exceptions.append(CustomWarning(tr("Regel %1 van schapen bestand bevat niet voldoende velden. Regel overgeslagen: \"%2\"").arg(linenum).arg(line)));
            continue;
        }

        Sheep s;
        
        QString uln = values[headers.lastIndexOf("dier_uln")].trimmed();
        s.setUln(uln);
        
        QString breederid = values[headers.lastIndexOf("ubn_fokker")].trimmed();
//        if (!newbreeders.contains(breederid)) 
//        {
//            exceptions.append(CustomWarning(tr("Fokker (%2) van schaap %1 komt niet voor in ledenbestand. (line %3)").arg(uln).arg(breederid).arg(linenum))); 
//            //continue;
//        }
        s.setBreeder(breederid);
        
        QString ownerid = values[headers.lastIndexOf("ubn_houder")].trimmed();
        s.setOwner(ownerid);
        if (!newbreeders.contains(ownerid))
        {
            if (skipped_sheep.contains(ownerid))
                skipped_sheep.insert(ownerid, skipped_sheep.value(ownerid)+1);
            else
                skipped_sheep.insert(ownerid, 1);
            continue;
        }
        
        
        s.setFather(values[headers.lastIndexOf("vader_stn")].trimmed());
        s.setMother(values[headers.lastIndexOf("moeder_stn")].trimmed());
        
        QString datestr  = normalizeDateString(values[headers.lastIndexOf("dier_geboortedatum")]);
        
        QRegularExpression dateformat1("^\\d\\d-\\d\\d-\\d\\d\\d\\d$");
        QRegularExpression dateformat2("^\\d\\d\\d\\d-\\d\\d-\\d\\d$");
        if (dateformat1.match(datestr).hasMatch())
        {
            s.setDateOfBirth(QDate::fromString(datestr, "dd-MM-yyyy"));
        }
        else if (dateformat2.match(datestr).hasMatch())
        {
            s.setDateOfBirth(QDate::fromString(datestr, "yyyy-MM-dd"));
        }
        else 
        {
            exceptions.append(CustomException(tr("Datum format niet herkend (%1). Gebruik jjjj-mm-dd, jjjj/mm/dd, dd-mm-jjjj, of dd/mm/jjjj").arg(values[headers.lastIndexOf("dier_geboortedatum")])));
        }
        
        
        
        QString stn = values[headers.lastIndexOf("dier_stn")].trimmed();
        if (stn.length() == 0) {
            exceptions.append(CustomWarning(tr("dier zonder stn gevonden op regel %1").arg(linenum)));
            continue;            
        }
        s.setStn(stn);
        
        s.setTailindex(QString("%1/%2").arg(values[headers.lastIndexOf("staartlen")]).arg(values[headers.lastIndexOf("staartlen_acc")]));
        
        if (values[headers.lastIndexOf("geslacht")].compare("Ram",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ram);
        else if (values[headers.lastIndexOf("geslacht")].compare("R",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ram);
        else if (values[headers.lastIndexOf("geslacht")].compare("Ooi",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ewe);
        else if (values[headers.lastIndexOf("geslacht")].compare("Ewe",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ewe);
        else if (values[headers.lastIndexOf("geslacht")].compare("E",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ewe);
        else if (values[headers.lastIndexOf("geslacht")].compare("O",Qt::CaseInsensitive)==0) s.setGender(Sheep::Ewe);
        else {
            exceptions.append(CustomWarning(tr("Het geslacht van %1 is verkeerd genoteerd (%2). Toegestane waarden zijn Ram, R, Ooi, Ewe, E en O.").arg(s.stn()).arg(values[headers.lastIndexOf("geslacht")])));
            continue;
        }
        
        if (newsheep.contains(s.stn()))
            exceptions.append(CustomWarning(tr("schaap met uln %1 komt meerdere keren voor in het bestand").arg(s.stn())));
        else
            newsheep.insert(s.stn(), s);
    }
    
    for (QString k : skipped_sheep.keys())
    {
        exceptions.append(CustomWarning(tr("Er zijn %1 schapen niet geimporteerd omdat de eigenaar (id %2) niet voorkomt in de ledenlijst. ").arg(skipped_sheep.value(k)).arg(k)));
    }
    
    // note that the QFile will automatically close the file when it's destructor is called
    // which will happen upon return. But also when one of the exceptions was throws.
//    sheepfile.close();
}

void EditShowWizard::on_EditShowWizard_currentIdChanged(int id)
{
    if (this->page(id) == ui->wizardPageSummary)
    {
        QList<CustomException> messages;
        try {
            // copy breeders:
            if (ui->radioButton_copyBreeders->isChecked())
            {
                if (ui->comboBox_copyFrom->currentIndex() < 0)
                {
                    throw CustomException(tr("Er is geen keuring geselecteerd terwijl er wel gekozen is om fokkers van een andere keuring te kopieren."));
                }
                Show other = ui->comboBox_copyFrom->currentData().value<Show>();
                newbreeders.clear();
                // if I understand correctly the copy-on-write behaviour of the qhash
                // will fork the list when items are removed/inserted but not when a sheep
                // in it is edited... so we need to do a deep copy
                for (Breeder b : other.breeders())
                {
                    // no cloning required as long as breeders contain no editable data
                    newbreeders.insert(b.id(), Breeder(b)); 
                }
            }
            else if (ui->radioButton_readBreeders->isChecked())
            {
                loadBreedersFromFile(ui->lineEdit_breedersFileName->text(), messages);
            }
            else {
                // the user can navigate back and forth so newbreeders may have been set to something else before
                newbreeders = m_show.breeders();
            }
            
            // sanity check:
            if (newbreeders.size()==0)
            {
                messages.append(CustomWarning(tr("Er is geen enkele fokker geimporteerd")));
            }
            
            // load or copy sheep:
            if (ui->radioButton_copySheep->isChecked())
            {
                if (ui->comboBox_copyFrom->currentIndex() < 0)
                {
                    throw CustomException(tr("Er is geen keuring geselecteerd terwijl er wel gekozen is om schapen van een andere keuring te kopieren."));
                }
                Show other = ui->comboBox_copyFrom->currentData().value<Show>();
                newsheep.clear();
                for (Sheep s : other.sheep())
                {
                    Sheep news = s.clone(); // clone the sheep, pun intended. this detaches the shared-pointers
                    news.reset();// clear all participation flags
                    newsheep.insert(s.stn(), news); 
                }
            }
            else if (ui->radioButton_readSheep->isChecked())
            {
                this->loadSheepFromFile(ui->lineEdit_sheepFileName->text(), messages);
            }
            else 
            {
//                 keep as is
                // this may seem futile but I was too afraid to screw something up
                // if newsheep gets assign the old sheep list this may make a shallow copy
                // and future edits to the list may damage the source show.
                // I think it should not because of copy-on-write behaviour of QHash but I didn't want to risk it
                newsheep.clear();
                for (Sheep& s : m_show.sheep()) {
                    newsheep.insert(s.stn(), s.clone());
//                    qDebug() << "copying sheep with id " << s.id() << " and shortname " << s.shortName() << "to newsheep list";
                }
//                newsheep = m_show.sheep();
            }
            
            
            // migrate registration information:
//            qDebug() << "migrating registration info from " << m_show.name() << "( " << m_show.sheep().size() << " sheep)";
            for (Sheep& s : m_show.sheep()) 
            {
//                qDebug() << "attempting registration transfer on " << s.shortName();
                if (s.doesParticipate())
                {
//                    qDebug() << "transferring participation status of " << s.shortName();
                    if (! newsheep.contains(s.stn())) 
                    {
                        Breeder owner = m_show.breeders().value(s.owner());
                        QString fullName = QString("%1 %2 %3").arg(owner.initials()).arg(owner.prefixes()).arg(owner.name()).simplified();
                        
                        messages.append(CustomWarning(tr("Schaap %1 wordt verwijderd maar was al ingeschreven voor een of meer onderdelen. Deze inschrijving zal ongedaan gemaakt worden.").arg(s.stn())));
                        if (s.doesPairOfRam()) 
                            messages.append(CustomWarning(tr("Hierdoor is de inschrijving voor het beste tweetal van een ram van %1 niet meer compleet!").arg(fullName)));
                        if (s.doesTripleOfRam()) 
                            messages.append(CustomWarning(tr("Hierdoor is de inschrijving voor het beste drietal van een ram van %1 niet meer compleet!").arg(fullName)));
                        if (s.doesCompanyThree()) 
                            messages.append(CustomWarning(tr("Hierdoor is de inschrijving voor het bedrijfsdrietal van %1 niet meer compleet!").arg(fullName)));
                        continue;    
                    }
                    // the sheep still exists, copy all user data
                    Sheep s2 = newsheep.value(s.stn());
                    s2.setRank(s.rank());
                    s2.setOrder(s.order());
                    s2.setPlace(s.place());
                    s2.setComment(s.comment());
                    s2.setSubcategory(s.subcategory());
                    s2.setDoesPairOfRam(s.doesPairOfRam());
                    s2.setDoesTripleOfRam(s.doesTripleOfRam());
                    s2.setDoesCompanyThree(s.doesCompanyThree());
                    s2.setChildPresentation(s.childPresentation());
                    s2.setDoesParticipate(s.doesParticipate());
                }
            }
            
            
            
            // sanity checks:
            // check name for non-empty and already existing
            if (ui->lineEdit_showName->text().isEmpty()) {
                throw CustomException(tr("De naam van de keuring mag niet leeg zijn. "));
            }
            for (Show s : Database::instance()->shows)
            {
                if (s == m_show) continue;
                if (s.name() == ui->lineEdit_showName->text())
                {
                    throw CustomException(tr("Er is al een keuring met de naam '%1'. ").arg(s.name()));
                }
            }
            // check date for past
            if (ui->dateEdit_showDate->date() < QDate::currentDate() )
            {
                messages.append(CustomWarning(tr("De datum is in het verleden")));
            }
            
            // if all is ok then produce statistics (number of sheep and breeders)
            messages.append(CustomAcceptance(tr("Er zijn %1 schapen en %2 fokkers ingelezen en klaar om opgeslagen te worden.").arg(newsheep.size()).arg(newbreeders.size())));
        } 
        catch (CustomException ex) {
            messages.append(ex);
        } 
        catch (std::exception ex)
        {
            messages.append(CustomException(tr("Er is een onverwacht probleem opgetreden: %1").arg(ex.what())));
        }
        catch(...)
        {
            messages.append(CustomException(tr("Er is een onbekend probleem opgetreden")));
        }
        this->button(QWizard::FinishButton)->setEnabled(true);
        ui->listWidget_summary->clear();
        for (CustomException e : messages)
        {
            QIcon icon = style()->standardIcon(e.iconstyle());
            QListWidgetItem * item = new QListWidgetItem(icon, e.what());
            // the item gets destroyed when the listwidget is destroyed
            // QIcon is one of the COW/implicitly shared data types so that this copy is destroyed after this scope finishes is no problem
            ui->listWidget_summary->addItem(item);
            if (e.isCritical())
            {
                this->button(QWizard::FinishButton)->setEnabled(false);
            }
        }
    }
}
