#include "registrationwizard.h"
#include "ui_registrationwizard.h"
#include "database.h"

#include <QComboBox>
#include <QStringList>
#include <QMap>
#include <QTableWidget>
#include <QLabel>
#include <QLineEdit>
#include <QCheckBox>
#include <QDebug>

#include <iostream>

using std::cout;
using std::endl;

RegistrationWizard::RegistrationWizard(Show show, QWidget *parent) :
    QWizard(parent),
    ui(new Ui::RegistrationWizard),
    m_show(show)
{
    // setup the interface:
    ui->setupUi(this);
    this->showMaximized();
    
    // determine unique owners:
    QMultiMap<QString, Breeder> map; // bit of a hack to get them in custom sorted order
    for (Breeder breeder : m_show.breeders()) {
        map.insert(breeder.name(), breeder);
    }
    
    // populate the actual list
    for (Breeder breeder : map)
    {
        QVariant breedervariant;
        breedervariant.setValue(breeder);
        qDebug() << tr("Appending breeder %1 to dropdown").arg(breeder.abbrv());
        ui->comboBox_breeder->addItem(QString("%3, %1 %2").arg(breeder.initials().trimmed()).arg(breeder.prefixes().trimmed()).arg(breeder.name().trimmed()), breedervariant);
    }
    ui->comboBox_breeder->setCurrentIndex(-1);
    hasgonebeyondpageone = false;
}


RegistrationWizard::~RegistrationWizard()
{
    delete ui;
}


void RegistrationWizard::on_comboBox_breeder_activated(int index)
{
//    m_dirty_sheep.clear();
    qDebug() << "breeder activated " << index;
                
    while (ui->table_sheepparticipating->rowCount() > 0) {
        ui->table_sheepparticipating->removeRow(0);
    }
    
    while (ui->tableWidget_children->rowCount() > 0)
    {
        ui->tableWidget_children->removeRow(0);
    }
    
    
    // determine the activated breeder:
    QVariant var = ui->comboBox_breeder->currentData();
    Breeder owner = var.value<Breeder>();
    
    // clone all sheep into the dirty array for editing
    QLocale locale  = QLocale(QLocale::Dutch, QLocale::Netherlands);
    
    QMultiMap<QDate, Sheep> sorted;
    for (Sheep s : m_show.sheep()) {
        sorted.insert(s.dateOfBirth(), s);
    }
    
    for (Sheep s : sorted.values())
    {
        if (s.owner() == owner.id())
        {
            //qDebug() << tr("making a row for %1(born %2)").arg(s.shortName()).arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy"));
//            Sheep s2 = s.clone();
//            m_dirty_sheep.insert(s2);
        
            // insert row in table
            QTableWidgetItem *newItem = new QTableWidgetItem(QString("%1 %2 %3").arg(Sheep::shortName(s.stn())).arg(locale.toString(s.dateOfBirth(), "d MMMM yyyy")).arg(s.gender()==Sheep::Ewe?"ooi":"ram"));
            newItem->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled); // and nothing else, this makes it non-draggable, non-droppable, non-editable, non-checkable
            QVariant sheepvar;
            sheepvar.setValue(s);
            newItem->setData(Qt::UserRole, sheepvar);
            int row = ui->table_sheepparticipating->rowCount();
            ui->table_sheepparticipating->insertRow(row);
            ui->table_sheepparticipating->setItem(row, 0, newItem);
            newItem->setSelected(s.doesParticipate());
        }
    }
    
    // make sure the comboboxes are populated correctly
    updateSheepComboBoxes();
    applyPreviouslySaved();
    // make at least one empty row
    makeChildRow();
}


void RegistrationWizard::ensurePresenceInComboBox(Sheep s, QComboBox* combobox, bool present) {
    QVariant sheepvar;
    sheepvar.setValue(s);
    int currentrow = combobox->findData(sheepvar);
    if (present && currentrow < 0) {
        bool wasempty = combobox->currentIndex()<0;
        combobox->addItem(Sheep::shortName(s.stn()), sheepvar);
        if (wasempty) combobox->setCurrentIndex(-1);        
    } else if( !present && currentrow >=0) {
        if (currentrow == combobox->currentIndex()) {
            combobox->setCurrentIndex(-1);
        }
        combobox->removeItem(currentrow);
    } else {
        //qDebug() << "unchanged sheep " << s.id();
    }
}

void RegistrationWizard::updateSheepComboBoxes() {
    for (int i=0; i < ui->table_sheepparticipating->rowCount() ; i++) {
        QTableWidgetItem * item = ui->table_sheepparticipating->item(i,0);
        QVariant sheepvar = item->data(Qt::UserRole);
        Sheep s = sheepvar.value<Sheep>();
        
        // assert presence in pair
        ensurePresenceInComboBox(s, ui->comboBox_pair1, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_pair2, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_triple1, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_triple2, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_triple3, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_company1, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_company2, item->isSelected());
        ensurePresenceInComboBox(s, ui->comboBox_company3, item->isSelected());
        // TODO ensurePresenceInComboBox(s, ui->comboBox_childSheep, item->isSelected());
    }    
}

void RegistrationWizard::applyPreviouslySaved() {
    // apply previous selections
    for (Sheep s : m_show.sheep())
    { 
        if (s.owner() != ui->comboBox_breeder->currentData().value<Breeder>().id()) {
            continue;
        }
        // child presentation:
        if (!(s.childPresentation().trimmed().isEmpty()))
        {
            ui->groupBoxChild->setChecked(true);
            QStringList childnames = s.childPresentation().split("|");
            for (QString child : childnames)
            {
                this->makeChildRow();
                // the above appends a row 
                int row = ui->tableWidget_children->rowCount()-1;
                QLineEdit * item = static_cast<QLineEdit*>(ui->tableWidget_children->cellWidget(row,0));
                QComboBox * combo= static_cast<QComboBox*>(ui->tableWidget_children->cellWidget(row,1));
                qDebug() << "restoring child presentation: " << s.stn() << ": " << child;
                item->setText(child);
                combo->setCurrentText(Sheep::shortName(s.stn()));
            }
        }
        
        // pair of ram:
        if (s.doesPairOfRam()) 
        {
            ui->groupBoxPair->setChecked(true);
            if (ui->comboBox_pair1->currentIndex() < 0)
            {
                ui->comboBox_pair1->setCurrentText(Sheep::shortName(s.stn()));
            } else {
                ui->comboBox_pair2->setCurrentText(Sheep::shortName(s.stn()));
            }
        }
        
        // triple of ram:
        if (s.doesTripleOfRam()) 
        {
            ui->groupBoxTriple->setChecked(true);
            if (ui->comboBox_triple1->currentIndex() < 0)
            {
                ui->comboBox_triple1->setCurrentText(Sheep::shortName(s.stn()));
            } else if (ui->comboBox_triple2->currentIndex() < 0){
                ui->comboBox_triple2->setCurrentText(Sheep::shortName(s.stn()));
            } else {
                ui->comboBox_triple3->setCurrentText(Sheep::shortName(s.stn()));
            }
        }
        
        // company threesome:
        if (s.doesCompanyThree()) 
        {
            ui->groupBoxCompany->setChecked(true);
            if (ui->comboBox_company1->currentIndex() < 0)
            {
                ui->comboBox_company1->setCurrentText(Sheep::shortName(s.stn()));
            } else if (ui->comboBox_company2->currentIndex() < 0){
                ui->comboBox_company2->setCurrentText(Sheep::shortName(s.stn()));
            } else {
                ui->comboBox_company3->setCurrentText(Sheep::shortName(s.stn()));
            }
        }
    }
}


void RegistrationWizard::on_RegistrationWizard_currentIdChanged(int id)
{
    if (id == 0 && hasgonebeyondpageone) {
        ui->label_warning->setText(tr("Pas op: als u wijzigingen heeft gemaakt in de volgende paginas gaan die verloren op het moment dat bovenstaande fokker wordt veranderd. "));
        ui->label_5->setPixmap(style()->standardPixmap(QStyle::SP_MessageBoxWarning));
        
    } else {
        hasgonebeyondpageone = true;
    }
    
    if (nextId() < 0)
    { // i.e. if this is the last page
       
        QList<CustomException> problems;
        // check that a company was selected
        if (ui->comboBox_breeder->currentIndex() < 0) {
            problems.append(CustomException(tr("Er is geen bedrijf geselecteerd dus er kan niets opgeslagen worden.")));
        }
        
        // check that special categories are full or deselected
        if (ui->groupBoxPair->isChecked()) {
            if (ui->comboBox_pair1->currentIndex() < 0 || ui->comboBox_pair2->currentIndex() < 0) {
                problems.append(CustomException(tr("De categorie 'beste tweetal van 1 ram' is actief maar er zijn minder dan 2 schapen voor geselecteerd.")));
            } else {
                // all sheep are real:
                Sheep s1 = ui->comboBox_pair1->currentData(Qt::UserRole).value<Sheep>();
                Sheep s2 = ui->comboBox_pair2->currentData(Qt::UserRole).value<Sheep>();
                if (s1.father() != s2.father() && !s1.father().trimmed().isEmpty() && !s2.father().trimmed().isEmpty()) {
                    problems.append(CustomWarning(tr("De schapen %1 en %2 doen mee voor het beste tweetal van een ram maar hebben niet dezelde vader.").arg(Sheep::shortName(s1.stn())).arg(Sheep::shortName(s2.stn()))));
                }
            }
            if (ui->comboBox_pair1->currentIndex() == ui->comboBox_pair2->currentIndex()) {
                problems.append(CustomException(tr("De twee schapen voor beste tweetal van 1 ram zijn hetzelfde.")));
            }
            
        }
        if (ui->groupBoxTriple->isChecked()) {
            if (ui->comboBox_triple1->currentIndex() < 0 || ui->comboBox_triple2->currentIndex() < 0 || ui->comboBox_triple3->currentIndex() < 0) {
                problems.append(CustomException(tr("De categorie 'beste drietal van 1 ram' is actief maar er zijn minder dan 3 schapen voor geselecteerd.")));
            } else {
                Sheep s1 = ui->comboBox_triple1->currentData(Qt::UserRole).value<Sheep>();
                Sheep s2 = ui->comboBox_triple2->currentData(Qt::UserRole).value<Sheep>();
                Sheep s3 = ui->comboBox_triple3->currentData(Qt::UserRole).value<Sheep>();
                if (!s1.father().trimmed().isEmpty() && !s2.father().trimmed().isEmpty() && !s3.father().trimmed().isEmpty()) {
                    if (s1.father()!=s2.father() || s2.father()!=s3.father() || s1.father()!=s3.father()) {
                        problems.append(CustomWarning(tr("De schapen %1, %2, en %3 doen mee voor het beste drietal van een ram maar hebben niet dezelde vader.").arg(Sheep::shortName(s1.stn())).arg(Sheep::shortName(s2.stn())).arg(Sheep::shortName(s3.stn()))));                        
                    }
                }
            }
            if (ui->comboBox_triple1->currentIndex() == ui->comboBox_triple2->currentIndex() ||
                ui->comboBox_triple2->currentIndex() == ui->comboBox_triple3->currentIndex() ||
                ui->comboBox_triple1->currentIndex() == ui->comboBox_triple3->currentIndex()) {
                problems.append(CustomException(tr("Er zijn twee dezelfde schapen geselecteerd voor beste drietal van 1 ram.")));
            }
        }
        if (ui->groupBoxCompany->isChecked()) {
            if (ui->comboBox_company1->currentIndex() < 0 || ui->comboBox_company2->currentIndex() < 0 || ui->comboBox_company3->currentIndex() < 0) {
                problems.append(CustomException(tr("De categorie 'bedrijfdrietal' is actief maar er zijn minder dan 3 schapen voor geselecteerd.")));
            }
            if (ui->comboBox_company1->currentIndex() == ui->comboBox_company2->currentIndex() ||
                ui->comboBox_company2->currentIndex() == ui->comboBox_company3->currentIndex() ||
                ui->comboBox_company1->currentIndex() == ui->comboBox_company3->currentIndex()) {
                problems.append(CustomException(tr("Er zijn twee dezelfde schapen geselecteerd voor het bedrijfsdrietal.")));
            }
        }
        
        // check that a sheep and child are set for the child presentation
        if (ui->groupBoxChild->isChecked()) {
            int numchildren=0;
            for (int row=0; row < ui->tableWidget_children->rowCount(); row++)
            {
                QLineEdit * item = static_cast<QLineEdit*>(ui->tableWidget_children->cellWidget(row,0));
                if ( ! item->text().isEmpty())
                {
                    numchildren++;
                }
                if (item->text().contains('|'))
                {
                    problems.append(CustomException("het karakter | mag niet gebruikt worden in de naam van het kind"));
                }
            }
            if (numchildren==0)
            {
                problems.append(CustomException(tr("De categorie presentatie door een kind is aangevinkt, maar er zijn 0 kinderen ingevoerd")));
            }
        }
        
        
        
        
        if (problems.size()==0)
        {
            problems.append(CustomAcceptance(tr("Klaar om op te slaan.")));
        }
       
        ui->wizardPageConfirmation->setStatus(problems);
    }
}

void RegistrationWizard::on_table_sheepparticipating_itemSelectionChanged()
{
    updateSheepComboBoxes();
}

void RegistrationWizard::on_RegistrationWizard_accepted()
{
    for (int i=0; i < ui->table_sheepparticipating->rowCount() ; i++) {
        QTableWidgetItem * item = ui->table_sheepparticipating->item(i,0);
        QVariant sheepvar = item->data(Qt::UserRole);
        Sheep s = sheepvar.value<Sheep>();
        
        // first we set all to false
        s.setDoesPairOfRam(false);
        s.setDoesTripleOfRam(false);
        s.setDoesCompanyThree(false);
        s.setChildPresentation("");
        
        // set participation from selected state in table view
        s.setDoesParticipate(item->isSelected());
    }
    
    
    // set special categories
    if (ui->groupBoxPair->isChecked()) {
          ui->comboBox_pair1->currentData(Qt::UserRole).value<Sheep>().setDoesPairOfRam(true);
          ui->comboBox_pair2->currentData(Qt::UserRole).value<Sheep>().setDoesPairOfRam(true);
    }
    if (ui->groupBoxTriple->isChecked()) {
          ui->comboBox_triple1->currentData(Qt::UserRole).value<Sheep>().setDoesTripleOfRam(true);
          ui->comboBox_triple2->currentData(Qt::UserRole).value<Sheep>().setDoesTripleOfRam(true);
          ui->comboBox_triple3->currentData(Qt::UserRole).value<Sheep>().setDoesTripleOfRam(true);
    }
    if (ui->groupBoxCompany->isChecked()) {
          ui->comboBox_company1->currentData(Qt::UserRole).value<Sheep>().setDoesCompanyThree(true);
          ui->comboBox_company2->currentData(Qt::UserRole).value<Sheep>().setDoesCompanyThree(true);
          ui->comboBox_company3->currentData(Qt::UserRole).value<Sheep>().setDoesCompanyThree(true);
    }
    
    if (ui->groupBoxChild->isChecked()) {
        for (int row=0; row < ui->tableWidget_children->rowCount(); row++)
        {
            QLineEdit * item = static_cast<QLineEdit*>(ui->tableWidget_children->cellWidget(row,0));
            QComboBox * combo= static_cast<QComboBox*>(ui->tableWidget_children->cellWidget(row,1));
            if (!item->text().isEmpty())
            {
                Sheep s = combo->currentData().value<Sheep>();
                QString child = s.childPresentation();
                if (child.isEmpty())
                {
                    child = item->text().trimmed();
                }
                else 
                {
                    child = child + "|" + item->text().trimmed();
                }
                qDebug() << "saving child presentation " << s.stn() << ": " << child;
                s.setChildPresentation(child);
            }
        }
    }

//    // and save to disk
    Database::instance()->saveShows();
}


void RegistrationWizard::makeChildRow()
{
    // make one extra empty row
    int row = ui->tableWidget_children->rowCount();
    qDebug() << "making a new row at " << row;
    ui->tableWidget_children->insertRow(row);
    
    QComboBox* combo = new QComboBox();
    ui->tableWidget_children->setCellWidget(row, 1, combo);
    for (Sheep s : m_show.sheep())
    {
        QVariant var = ui->comboBox_breeder->currentData();
        Breeder owner = var.value<Breeder>();
        if (s.owner() == owner.id())
        {
            QVariant var;
            var.setValue(s);
            combo->addItem(Sheep::shortName(s.stn()), var);
        }
    }
    
    QLineEdit * item = new QLineEdit();
    connect(item, &QLineEdit::editingFinished, [=]() {
        qDebug() << "table cell edited: "<< item->text();
        if (item->text().isEmpty())
        {
            qDebug() << "row empty, should be removed";
            for (int row2 =0; row2 < ui->tableWidget_children->rowCount() - 1; row2++)
            {
                QLineEdit* lineedit = static_cast<QLineEdit*>(ui->tableWidget_children->cellWidget(row2, 0));    
                if (item==lineedit)
                {
                    qDebug() << "removing empty row at " << row2;
                    ui->tableWidget_children->removeRow(row2);
                }
            }
        }
        // if the last row is not empty, add another row
        int lastrow = ui->tableWidget_children->rowCount() - 1;
        QLineEdit* lineedit = static_cast<QLineEdit*>(ui->tableWidget_children->cellWidget(lastrow, 0));    
        if ( ! lineedit->text().isEmpty())
        {
            this->makeChildRow();
        }
    });
    connect(item, &QLineEdit::returnPressed, [=](){
        qDebug() << "enter caught";
    });
    ui->tableWidget_children->setCellWidget(row,0, item);
    
}

void RegistrationWizard::on_tableWidget_children_itemChanged(QTableWidgetItem *item)
{
//    qDebug() << "item changed";
    
    //ensureOneEmptyRowInChildList();
}


void RegistrationWizard::on_tableWidget_children_cellChanged(int row, int column)
{
//    qDebug() << "cell changed " << row << " " << column;
}

void RegistrationWizard::on_tableWidget_children_currentCellChanged(int currentRow, int currentColumn, int previousRow, int previousColumn)
{
//    qDebug() << "current cell changed";
}
