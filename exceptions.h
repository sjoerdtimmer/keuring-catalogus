#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>
#include <QString>
#include <QStyle>

class CustomException
{
protected:
   QString s;
   bool critical;
   QStyle::StandardPixmap pixmap;

public:
   CustomException(QString ss);
   QString what();
   bool isCritical();
   QStyle::StandardPixmap iconstyle();
};


class CustomWarning : public CustomException {
public:
    CustomWarning(QString ss);
};

class CustomNotice : public CustomException {
public:
    CustomNotice(QString ss);
};

class CustomAcceptance : public CustomException {
public:
    CustomAcceptance(QString ss);
};



#endif // EXCEPTIONS_H
