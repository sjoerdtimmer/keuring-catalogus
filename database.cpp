#include "database.h"
#include "datastructures/show.h"
#include "exceptions.h"

#include <QStandardPaths>
#include <QFile>
#include <QMessageBox>
#include <QFileInfo>
#include <QDir>
#include <QTextStream>
#include <QRegExp>
#include <QDirIterator>
#include <QStringBuilder>
#include <QString>
#include <QtDebug>
#include <QStandardPaths>
#include <QHash> 

#include <iostream>

using std::cout;
using std::endl;

Database::Database()
{

}

QString Database::sanitizeShowFileName(Show& show)
{
    // determine file location:
    QStringList appdatalocations = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    QRegularExpression rx("[^A-Za-z0-9]");
    return appdatalocations.at(0) % QDir::separator() % (show.name().replace(rx,"")) % ".show";
}


QString Database::nextAvailableName()
{
    QSet<QString> names;
    for (Show& s : shows)
    {
        names.insert(s.name());
    }
    int i = 0;
    QString name;
    do 
    {
        name = tr("Nieuwe keuring %1").arg(++i);
    }    while(names.contains(name));
    return name;
    
}

void Database::saveShows()
{
    // make sure appdata dir exists
    QStringList appdatalocations = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    QDir().mkpath(appdatalocations.at(0));

    QSet<QString> shownames;
    
    // iterate over shows
    for (Show& show : shows)
    {
        QString fileName = sanitizeShowFileName(show);
        shownames.insert(QFileInfo(fileName).fileName());
        QString tmpFile  = appdatalocations.at(0) % QString("/tmpshow");
        
        // open the temp file
        QFile file(tmpFile);
        qDebug() << "tmp file " << tmpFile;
        if (! file.open(QIODevice::WriteOnly)) {
            QMessageBox::information(0, "error", file.errorString());
            continue;
        }
        
        // serialize the data
        QDataStream out(&file);
        
        // Write a header with a "magic number" and a version
        out << (quint32)0xA0B0C0D0;
        out.setVersion(QDataStream::Qt_5_10);
        
        // Write the data
        out << show;
        
        file.close();
        // atomic move
        qDebug() << "clearing destination file name " << fileName;
        QFile(fileName).remove();
        qDebug() << "moving temp file to " << fileName;
        file.rename(fileName);
    }
    
    // remove old show files, this should not be needed
    QDirIterator it(appdatalocations.at(0));
    while (it.hasNext())
    {
        QString fileName = it.next();
        if (! fileName.endsWith(".show")) continue;
        if (shownames.contains(QFileInfo(fileName).fileName())) continue;
        
        // if not one that was just written, then remove
        qDebug() << "removing" << fileName;
        QFile file (fileName);
        file.remove();
    }
}



void Database::loadShows()
{
    QStringList appdatalocations = QStandardPaths::standardLocations(QStandardPaths::AppDataLocation);
    if (appdatalocations.size()==0) return; // guaranteed to never happen

    QDirIterator it(appdatalocations.at(0));
    while (it.hasNext())
    {
        QString fileName = it.next();
        if (! fileName.endsWith(".show")) continue;
        // open the file
        QFile file(fileName);
        if (! file.open(QIODevice::ReadOnly)) {
            QMessageBox::information(0,"error",file.errorString());
            file.close();
            continue;
        }
        
        QDataStream in(&file);
        
        // Read and check the header
        quint32 magic;
        in >> magic;
        if (magic != 0xA0B0C0D0) 
        {
            cout << "magic mismatch!" << endl;
            continue;
        }
        in.setVersion(QDataStream::Qt_5_10);
        
        // Read the data
        Show s;
        in >> s;
        
        qDebug() << QString("show '%1' read, on date %2 containing %3 sheep from %4 breeders")
                    .arg(s.name())
                    .arg(s.date().toString())
                    .arg(s.sheep().size())
                    .arg(s.breeders().size());
        
        file.close();
        qDebug() << "appending show to database list";
        shows.append(s);
        qDebug() << "emitting trigger now";
        emit showRead(s);
        qDebug() << "done loading shows";
    }
}



//void Database::loadSheepFromStream(QTextStream  &in, QHash<QString,Breeder> &breederlist, QHash<QString,Sheep> &sheeplist, QString delimiter)
//{
    
//}
